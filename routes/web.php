<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Http\Request;

// Route::get('/', function () {
//    return view('welcome');
//    dd(\Illuminate\Support\Facades\Auth::guard('admin')->check());
    // dd(\Illuminate\Support\Facades\Auth::guard('admin')->user());
// });
// Route::pattern('id', '[0-9]+');
// Route::pattern('params', '[a-zA-Z0-9]+');
Route::get('/',['uses'=>'IndexController@index']);
Route::get('/about',['uses'=>'AboutController@index']);
Route::group(['prefix'=>'requirement'],function(){
    Route::get('/',['uses'=>'RequirementController@index']);
    Route::get('/{params}.html',['uses'=>'RequirementController@detail']);
});

Route::group(['prefix'=>'news'],function(){
    Route::get('/',['uses'=>'NewsController@index']);
    Route::get('/{params}.html',['uses'=>'NewsController@detail']);
});
Route::group(['prefix'=>'services'],function(){
    Route::get('/',['uses'=>'ServiceController@index']);
    Route::get('/{params}.html',['uses'=>'ServiceController@detail']);
});
Route::get('/contact',['uses'=>'ContactController@index']);
Route::post('/language_set',['uses'=>'IndexController@language_set'])->name('language_set');
Route::get('/post',['uses'=>'IndexController@get_posts']);
