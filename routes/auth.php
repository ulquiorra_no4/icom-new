<?php
/**
 * Created by PhpStorm.
 * User: HiepHM
 * Date: 11/8/2017
 * Time: 2:28 PM
 */
Route::get('register',['uses'=>'AdRegisterController@showRegisterForm']);
Route::post('register',['uses'=>'AdRegisterController@register'])->name('register');
Route::get('login',['uses'=>'AdLoginController@showLogin']);
Route::post('login',['uses'=>'AdLoginController@login'])->name('login');
Route::post('logout',['uses'=>'AdLoginController@logout'])->name('logout');