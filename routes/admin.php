<?php
/**
 * Created by PhpStorm.
 * User: HiepHM
 * Date: 11/8/2017
 * Time: 1:34 PM
 * |--------------------------------------------------------------------------
 * | Admin Routes
 * |--------------------------------------------------------------------------
 */

Route::get('/', ['uses' => 'DashboardController@index']);
//Route::get('/',function (){
//    dd(\Illuminate\Support\Facades\Auth::guard('admin')->user());
//});

Route::group(['prefix' => 'modules', 'name' => 'modules'], function () {
    Route::get('/', ['uses' => 'ModuleController@index'])->name('mod');
    Route::get('add', ['uses' => 'ModuleController@create'])->name('mod_add');
    Route::post('store', ['uses' => 'ModuleController@store'])->name('mod_store');
    Route::get('edit/{id}', ['uses' => 'ModuleController@edit'])->name('mod_edit');
    Route::patch('update/{id}', ['uses' => 'ModuleController@update'])->name('mod_update');
    Route::post('destroy/{id}', ['uses' => 'ModuleController@destroy'])->name('mod_destroy');
    Route::post('multi_destroy', ['uses' => 'ModuleController@multi_destroy'])->name('mod_multi_destroy');
});
Route::group(['prefix' => 'categories', 'name' => 'categories'], function () {
    Route::get('/', ['uses' => 'CategoryController@index'])->name('cat');
    Route::get('add', ['uses' => 'CategoryController@create'])->name('cat_add');
    Route::post('store', ['uses' => 'CategoryController@store'])->name('cat_store');
    Route::get('edit/{id}', ['uses' => 'CategoryController@edit'])->name('cat_edit');
    Route::patch('update/{id}', ['uses' => 'CategoryController@update'])->name('cat_update');
    Route::post('destroy/{id}', ['uses' => 'CategoryController@destroy'])->name('cat_destroy');
    Route::post('multi_destroy', ['uses' => 'CategoryController@multi_destroy'])->name('cat_multi_destroy');
});
Route::group(['prefix'=>'posts','name'=>'posts'],function(){
    Route::get('/',['uses'=>'PostController@index'])->name('pos');
    Route::get('add',['uses'=>'PostController@create'])->name('pos_add');
    Route::post('store',['uses'=>'PostController@store'])->name('pos_store');
    Route::get('edit/{id}',['uses'=>'PostController@edit'])->name('pos_edit');
    Route::patch('update/{id}',['uses'=>'PostController@update'])->name('pos_update');
    Route::post('destroy/{id}',['uses'=>'PostController@destroy'])->name('pos_destroy');
    Route::post('multi_destroy',['uses'=>'PostController@multi_destroy'])->name('pos_multi_destroy');
});
Route::group(['prefix'=>'menuses','name'=>'menuses'],function(){
    Route::get('/',['uses'=>'MenuController@index'])->name('menu');
    Route::get('add',['uses'=>'MenuController@create'])->name('menu_add');
    Route::post('store',['uses'=>'MenuController@store'])->name('menu_store');
    Route::get('edit/{id}',['uses'=>'MenuController@edit'])->name('menu_edit');
    Route::patch('update/{id}',['uses'=>'MenuController@update'])->name('menu_update');
    Route::post('destroy/{id}',['uses'=>'MenuController@destroy'])->name('menu_destroy');
    Route::post('multi_destroy',['uses'=>'MenuController@multi_destroy'])->name('menu_multi_destroy');
});
Route::group(['prefix'=>'infosites','name'=>'infosites'],function(){
    Route::get('/',['uses'=>'InfoSiteController@index'])->name('inf');
    Route::get('add',['uses'=>'InfoSiteController@create'])->name('inf_add');
    Route::post('store',['uses'=>'InfoSiteController@store'])->name('inf_store');
    Route::get('edit/{id}',['uses'=>'InfoSiteController@edit'])->name('inf_edit');
    Route::patch('update/{id}',['uses'=>'InfoSiteController@update'])->name('inf_update');
    Route::post('destroy/{id}',['uses'=>'InfoSiteController@destroy'])->name('inf_destroy');
    Route::post('multi_destroy',['uses'=>'InfoSiteController@multi_destroy'])->name('inf_multi_destroy');
});
Route::group(['prefix'=>'services','name'=>'services'],function(){
    Route::get('/',['uses'=>'ServiceController@index'])->name('ser');
    Route::get('add',['uses'=>'ServiceController@create'])->name('ser_add');
    Route::post('store',['uses'=>'ServiceController@store'])->name('ser_store');
    Route::get('edit/{id}',['uses'=>'ServiceController@edit'])->name('ser_edit');
    Route::patch('update/{id}',['uses'=>'ServiceController@update'])->name('ser_update');
    Route::post('destroy/{id}',['uses'=>'ServiceController@destroy'])->name('ser_destroy');
    Route::post('multi_destroy',['uses'=>'ServiceController@multi_destroy'])->name('ser_multi_destroy');
});
