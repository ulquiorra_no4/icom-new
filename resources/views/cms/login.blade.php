<!DOCTYPE html>
<html lang=en>
<head>
    <meta charset=utf-8>
    <meta content="IE=edge" http-equiv=X-UA-Compatible>
    <meta content="width=device-width,initial-scale=1" name=viewport>
    <meta content="CMS" name=description>
    <title>CMS Blog</title>
    <link href="{{asset('css/bootstrap.min.css')}}" rel=stylesheet>
    <link href="{{asset('font-awesome/4.5.0/css/font-awesome.min.css')}}" rel=stylesheet>
    <link href="{{asset('css/fonts.googleapis.com.css')}}" rel=stylesheet>
    <link href="{{asset('css/ace.min.css')}}" rel=stylesheet>
    <link href="{{asset('css/admin.css')}}" rel=stylesheet>
</head>
<body class="no-skin">
<div class="main-container ace-save-state" id="main-container">
    <div class="main-content">
        <div class="main-content-inner">
            <div class="col-sm-10 col-sm-offset-1">
                <div class="login-container">
                    <div class="position-relative">
                        <div id="login-box" class="login-box visible widget-box no-border">
                            <div class="widget-body">
                                <div class="widget-main">
                                    <h4 class="header blue lighter bigger">
                                        <i class="ace-icon fa fa-lock green"></i>
                                        Administrator
                                    </h4>
                                    <div class="space-6"></div>
                                    <form class="form-horizontal" role="form" method="POST" action="{{route('login')}}">
                                        {{ csrf_field() }}
                                        <fieldset>
                                            <label class="block clearfix">
                                                <span class="block input-icon input-icon-right">
                                                    <input type="text" name="username" class="form-control" placeholder="Username" required autofocus/>
                                                    <i class="ace-icon fa fa-user"></i>
                                                </span>
                                            </label>
                                            @if ($errors->has('username'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('username') }}</strong>
                                            </span>
                                            @endif
                                            <label class="block clearfix">
                                                <span class="block input-icon input-icon-right">
                                                    <input type="password" name="password" class="form-control" placeholder="Password" required autofocus/>
                                                    <i class="ace-icon fa fa-lock"></i>
                                                </span>
                                            </label>
                                            @if ($errors->has('password'))
                                                <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                            @endif
                                            <div class="clearfix">
                                                <label class="inline">
                                                    <input type="checkbox" class="ace" name="remember" {{ old('remember') ? 'checked' : '' }}/>
                                                    <span class="lbl"> Remember Me</span>
                                                </label>
                                                <button class="width-35 pull-right btn btn-sm btn-primary">
                                                    <i class="ace-icon fa fa-key"></i>
                                                    <span class="bigger-110">Login</span>
                                                </button>
                                            </div>
                                        </fieldset>
                                    </form>
                                </div><!-- /.widget-main -->
                            </div><!-- /.widget-body -->
                        </div><!-- /.login-box -->
                    </div><!-- /.position-relative -->
                </div>
            </div><!-- /.col -->
        </div>
    </div><!-- /.main-content -->
</div><!-- /.main-container -->
</body>
</html>