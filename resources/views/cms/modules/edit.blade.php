@extends('layouts.admin')
@section('breadcrumb')
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="/admin">Admin</a>
            </li>
            <li><a href="{{route('mod')}}">Module</a></li>
            <li class="active">Edit/{{$module->mod_id}}</li>
        </ul><!-- /.breadcrumb -->
    </div>
@stop
@section('content')
    <div class="row">
        <div class="col-xs-12">
            <h4 class="lighter block green lighter-heading"><span class="red-bold">(*)</span>{{trans('label.note.required')}}</h4>
            @if(Session::has('alert_success'))
                <div class="alert alert-block alert-success">
                    <button type="button" class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    <i class="ace-icon fa fa-check green"></i>
                    {{Session::get('alert_success')}}
                </div>
            @endif
            {!! Form::model($module,['method'=>'PATCH','action'=>['Cms\ModuleController@update',$module->mod_id],'class'=>'form-horizontal','role'=>'form']) !!}
                @include('cms.modules.form',['submit'=>trans('label.update'),'module'=>$module])
            {!! Form::close() !!}
        </div>
    </div>
@stop
@section('script')

@stop
