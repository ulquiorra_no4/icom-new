<div class="row">
    <div class="col-md-8">
        <div class="form-group {{$errors->has('ser_title')?'has-error':''}}">
            <label for="ser_title">{{trans('label.service.title')}} <span class="red-bold">(*)</span></label>
            {!! Form::text('ser_title',old('ser_title'),['class'=>'col-xs-12 form-control','id'=>'ser_title','required','placeholder'=>trans('label.service.title')]) !!}
            @if ($errors->has('ser_title'))
                <p class="help-block col-sm-12">{{$errors->first('ser_title')}}</p>
            @endif
        </div>
        <div class="form-group {{$errors->has('ser_title_en')?'has-error':''}}">
            <label for="ser_title_en">{{trans('label.service.titleEn')}}</label>
            {!! Form::text('ser_title_en',old('ser_title_en'),['class'=>'col-xs-12 form-control','id'=>'ser_title_en','required','placeholder'=>trans('label.service.titleEn')]) !!}
        </div>
        <div class="form-group">
            <label for="ser_desc">{{trans('label.service.description')}}</label>
            {!! Form::textarea('ser_description',old('ser_description'),['class'=>'col-xs-12 form-control','id'=>'ser_desc','rows'=>3]) !!}
        </div>
        <div class="form-group">
            <label for="ser_desc_en">{{trans('label.service.descriptionEn')}}</label>
            {!! Form::textarea('ser_description_en',old('ser_description_en'),['class'=>'col-xs-12 form-control','id'=>'ser_desc_en','rows'=>3]) !!}
        </div>
        <div class="form-group">
            <label for="ser_quote">{{trans('label.service.quote')}}</label>
            {!! Form::textarea('ser_quote',old('ser_quote'),['class'=>'col-xs-12 form-control','id'=>'ser_quote','rows'=>3]) !!}
        </div>
        <div class="form-group">
            <label for="ser_quote_en">{{trans('label.service.quoteEn')}}</label>
            {!! Form::textarea('ser_quote_en',old('ser_quote_en'),['class'=>'col-xs-12 form-control','id'=>'ser_quote_en','rows'=>3]) !!}
        </div>
        <div class="form-group {{$errors->has('ser_content')?'has-error':''}}">
            <label for="ser_content">{{trans('label.service.content')}}</label>
            {!! Form::textarea('ser_content',old('ser_content'),['class'=>'col-xs-12 form-control','id'=>'ser_content','rows'=>7]) !!}
        </div>
        <div class="form-group {{$errors->has('ser_content_en')?'has-error':''}}">
            <label for="ser_content_en">{{trans('label.service.contentEn')}}</label>
            {!! Form::textarea('ser_content_en',old('ser_content_en'),['class'=>'col-xs-12 form-control','id'=>'ser_content_en','rows'=>7]) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group {{$errors->has('ser_avatar')?'has-error':''}}">
            <label for="ser_avatar">{{trans('label.service.avatar')}}</label>
            {!! Form::file('ser_avatar',['class'=>'col-xs-12','id'=>'ser_avatar','multiple'=>'']) !!}
            @if ($errors->has('ser_avatar'))
                <p class="help-block col-sm-12">{{$errors->first('ser_avatar')}}</p>
            @endif
        </div>
        <div class="form-group {{$errors->has('ser_thumbnail')?'has-error':''}}">
            <label for="ser_thumbnail">{{trans('label.service.image')}}</label>
            {!! Form::file('ser_thumbnail',['class'=>'col-xs-12','id'=>'ser_thumbnail','multiple'=>'']) !!}
            @if ($errors->has('ser_thumbnail'))
                <p class="help-block col-sm-12">{{$errors->first('ser_thumbnail')}}</p>
            @endif
        </div>
        <div class="form-group {{$errors->has('order')?'has-error':''}}">
            <label for="ser_order">{{trans('label.service.order')}}</label>
            {!! Form::text('order',old('order'),['class'=>'col-xs-12 form-control','id'=>'ser_order','placeholder'=>trans('label.service.order')]) !!}
        </div>
        <div class="form-group">
            <label for="ser_active" class="col-sm-6 control-label no-padding-right">{{trans('label.service.active')}}</label>
            <div class="col-sm-6">
                <div class="gr-act">
                    {!! Form::checkbox('active',1,old('active'),['class'=>'ace ace-switch ace-switch-3','id'=>'ser_active','checked']) !!}
                    <span class="lbl"></span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="ser_hot" class="col-sm-6 control-label no-padding-right">{{trans('label.service.hot')}}</label>
            <div class="col-sm-6">
                <div class="gr-act">
                    {!! Form::checkbox('hot',1,old('hot'),['class'=>'ace ace-switch ace-switch-3','id'=>'ser_hot']) !!}
                    <span class="lbl"></span>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="ser_homepage" class="col-sm-6 control-label no-padding-right">{{trans('label.service.home')}}</label>
            <div class="col-sm-6">
                <div class="gr-act">
                    {!! Form::checkbox('set_home',1,old('set_home'),['class'=>'ace ace-switch ace-switch-3','id'=>'ser_homepage']) !!}
                    <span class="lbl"></span>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix form-actions">
    <div class="col-md-offset-3 col-md-9">
        <button class="btn btn-info">
            <i class="ace-icon fa fa-check bigger-110"></i>
            {{$submit}}
        </button>
        <button class="btn" type="reset">
            <i class="ace-icon fa fa-undo bigger-110"></i>
            {{trans('label.reset')}}
        </button>
    </div>
</div>
@section('script')
<script src="{{asset('plugins/summernote/summernote-bs4.js')}}"></script>
<script type="text/javascript">
    jQuery(function($) {
        $('#ser_content').summernote();
        $('#ser_content_en').summernote();
    });
</script>
@stop
