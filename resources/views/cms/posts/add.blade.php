@extends('layouts.admin')
@section('breadcrumb')
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
        <ul class="breadcrumb">
            <li>
                <i class="ace-icon fa fa-home home-icon"></i>
                <a href="/admin">Admin</a>
            </li>
            <li><a href="{{route('pos')}}">Posts</a></li>
            <li class="active">Add</li>
        </ul><!-- /.breadcrumb -->
    </div>
@stop

@section('content')
    <div class="row">
        <div class="col-xs-12">
            @if(Session::has('alert_success'))
                <div class="alert alert-block alert-success">
                    <button type="button" class="close" data-dismiss="alert">
                        <i class="ace-icon fa fa-times"></i>
                    </button>
                    <i class="ace-icon fa fa-check green"></i>
                    {{Session::get('alert_success')}}
                </div>
            @endif
            <h4 class="lighter block green lighter-heading"><span class="red-bold">(*)</span> {{trans('label.note.required')}}</h4>
            {!! Form::open(['action'=>'Cms\PostController@store','class'=>'form','role'=>'form','method'=>'POST','enctype' => 'multipart/form-data']) !!}
            @include('cms.posts.form',['submit'=>trans('label.add')])
            {!! Form::close() !!}
        </div>
    </div>
@stop
@section('style')
    <link rel="stylesheet" href="{{asset('plugins/froala-editor/css/froala_editor.pkgd.css')}}" />
    <link rel="stylesheet" href="{{asset('plugins/froala-editor/css/froala_style.css')}}" />
@stop
@section('script')
    <script src="{{asset('js/bootstrap-tag.min.js')}}"></script>
    <script src="{{asset('js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{asset('js/bootstrap-timepicker.min.js')}}"></script>
    <script src="{{asset('js/bootstrap-datetimepicker.min.js')}}"></script>
    <script src="{{asset('plugins/froala-editor/js/froala_editor.pkgd.min.js')}}"></script>
    <script>
        $(function() {
            var a_cat = {!! $a_categories->toJson(); !!};
            $('#pos_content').froalaEditor();
            $('.fr-box div[style]')[0].remove();
            var tag_keyword = $('#pos_keyword');
            try{
                tag_keyword.tag(
                    {
                        placeholder:tag_keyword.attr('placeholder'),
                        source: a_cat,//defined in ace.js >> ace.enable_search_ahead
                    }
                )
                //programmatically add/remove a tag
                var $tag_obj2 = $('#pos_tags').data('tag');
                var index2 = $tag_obj2.inValues('some tag');
                $tag_obj2.remove(index2);
            }catch(e){}
            var tag_cate = $('#pos_cat_tag');
            try{
                tag_cate.tag({
                    placeholder:tag_cate.attr('placeholder'),
                    source: a_cat,//defined in ace.js >> ace.enable_search_ahead
                })
                //programmatically add/remove a tag
                var $tag_obj3 = tag_cate.data('tag');
                var index3 = $tag_obj3.inValues('news tag');
                $tag_obj3.remove(index3);
            }catch(e){}
            var tag_input = $('#pos_tags');
            try{
                tag_input.tag(
                    {
                        placeholder:tag_input.attr('placeholder'),
                    }
                )
                //programmatically add/remove a tag
                var $tag_obj = $('#pos_tags').data('tag');
                var index = $tag_obj.inValues('some tag');
                $tag_obj.remove(index);
            }
            catch(e) {
                tag_input.after('<textarea id="'+tag_input.attr('id')+'" name="'+tag_input.attr('name')+'" rows="3">'+tag_input.val()+'</textarea>').remove();
            }
            var tag_option = $('#pos_options');
            try{
                tag_option.tag({
                    placeholder:tag_option.attr('placeholder'),
                })
                //programmatically add/remove a tag
                var $tag_obj4 = tag_option.data('tag');
                var index4 = $tag_obj4.inValues('news tag');
                $tag_obj4.remove(index4);
            }catch(e){}
            $('#pos_active_time').datetimepicker({
                icons: {
                    time: 'fa fa-clock-o',
                    date: 'fa fa-calendar',
                    up: 'fa fa-chevron-up',
                    down: 'fa fa-chevron-down',
                    previous: 'fa fa-chevron-left',
                    next: 'fa fa-chevron-right',
                    today: 'fa fa-arrows ',
                    clear: 'fa fa-trash',
                    close: 'fa fa-times'
                }
            }).next().on(ace.click_event, function(){
                $(this).prev().focus();
            });
            $('#pos_avatar').ace_file_input({
                style: 'well',
                btn_choose: 'Drop files here or click to choose',
                btn_change: null,
                no_icon: 'ace-icon fa fa-cloud-upload',
                droppable: true,
                thumbnail: 'small'//large | fit
                ,
                preview_error : function(filename, error_code) {
                }

            }).on('change', function(){
                console.log($(this).data('ace_input_files'));
            });
        });
    </script>
@stop
