<div class="row">
    <div class="col-md-8">
        <div class="form-group {{$errors->has('pos_title')?'has-error':''}}">
            <label for="pos_title">{{trans('label.post.title')}} <span class="red-bold">(*)</span></label>
            {!! Form::text('title',old('title'),['class'=>'col-xs-12 form-control','id'=>'pos_title','required','placeholder'=>trans('label.post.title')]) !!}
            @if ($errors->has('title'))
                <p class="help-block col-sm-12">{{$errors->first('title')}}</p>
            @endif
        </div>
        <div class="form-group">
            <label for="pos_title_en">{{trans('label.post.title')}}</label>
            {!! Form::text('title_en',old('title_en'),['class'=>'col-xs-12 form-control','id'=>'pos_title_en','placeholder'=>trans('label.post.title')]) !!}
            @if ($errors->has('title_en'))
                <p class="help-block col-sm-12">{{$errors->first('title_en')}}</p>
            @endif
        </div>
        <div class="form-group">
            <label for="pos_desc">{{trans('label.post.description')}}</label>
            {!! Form::textarea('description',old('description'),['class'=>'col-xs-12 form-control','id'=>'pos_desc','rows'=>4]) !!}
            @if ($errors->has('description'))
                <p class="help-block col-sm-12">{{$errors->first('description')}}</p>
            @endif
        </div>
        <div class="form-group">
            <label for="pos_keyword">{{trans('label.post.keywords')}}</label>
            <div class="tag-group">
                {!! Form::text('keywords',old('keywords'),['class'=>'col-xs-12 form-control','id'=>'pos_keyword','placeholder'=>trans('label.post.keywords')]) !!}
            </div>
        </div>
        <div class="form-group {{$errors->has('content')?'has-error':''}}">
            <label for="pos_content">{{trans('label.post.content')}} <span class="red-bold">(*)</span></label>
            {!! Form::textarea('content',old('content'),['class'=>'col-xs-12 form-control','id'=>'pos_content','rows'=>10,'required']) !!}
            @if ($errors->has('content'))
                <p class="help-block col-sm-12">{{$errors->first('content')}}</p>
            @endif

        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group {{$errors->has('avatar')?'has-error':''}}">
            <label for="pos_avatar">{{trans('label.post.avatar')}} <span class="red-bold">(*)</span></label>
            {!! Form::file('avatar',['class'=>'col-xs-12','id'=>'pos_avatar','multiple'=>'']) !!}
            @if ($errors->has('avatar'))
                <p class="help-block col-sm-12">{{$errors->first('avatar')}}</p>
            @endif
        </div>
        <div class="form-group">
            <label for="pos_cat_id">{{trans('label.post.catId')}}</label>
            {!! Form::select('category_id',$a_categories,old('category_id'),['class'=>'col-xs-12 chosen-select form-control','id'=>'pos_cat_id']) !!}
        </div>
        <div class="form-group">
            <label for="pos_cat_tag">{{trans('label.post.catTag')}}</label>
            <div class="tag-group">
                {!! Form::text('cate_tags',old('cate_tags'),['class'=>'col-xs-12 form-control','id'=>'pos_cat_tag','placeholder'=>trans('label.post.catTag')]) !!}
            </div>
        </div>
        <div class="form-group">
            <label for="pos_tags">{{trans('label.post.tag')}}</label>
            <div class="tag-group">
                {!! Form::text('tags',old('tags'),['class'=>'col-xs-12 form-control','id'=>'pos_tags','placeholder'=>trans('label.post.tag')]) !!}
            </div>
        </div>
        <div class="form-group {{$errors->has('source')?'has-error':''}}">
            <label for="pos_source">{{trans('label.post.source')}}</label>
            {!! Form::text('source',old('source'),['class'=>'col-xs-12 form-control','id'=>'pos_source','placeholder'=>trans('label.post.source')]) !!}
            @if ($errors->has('source'))
                <p class="help-block col-sm-12">{{$errors->first('source')}}</p>
            @endif
        </div>
        <div class="form-group">
            <label for="pos_sou_url">{{trans('label.post.sourceUrl')}}</label>
            {!! Form::text('source_url',old('source_url'),['class'=>'col-xs-12 form-control','id'=>'pos_sou_url','placeholder'=>trans('label.post.sourceUrl')]) !!}
        </div>
        <div class="form-group">
            <label for="pos_active_time">{{trans('label.post.activeTime')}}</label>
            {!! Form::text('active_time',old('active_time'),['class'=>'col-xs-12 form-control','id'=>'pos_active_time','placeholder'=>trans('label.post.activeTime')]) !!}
        </div>
        <div class="form-group">
            <label for="pos_options">{{trans('label.post.option')}}</label>
            <div class="tag-group">
                {!! Form::text('option',old('option'),['class'=>'col-xs-12 form-control','id'=>'pos_options','placeholder'=>trans('label.post.option')]) !!}
            </div>
        </div>
        <div class="form-group">
            <label for="pos_partner">{{trans('label.post.partner')}}</label>
            {!! Form::select('partner_id',$a_categories,old('partner_id'),['class'=>'col-xs-12 chosen-select form-control','id'=>'pos_partner']) !!}
        </div>
        <div class="form-group">
            <label for="pos_active" class="col-sm-3 control-label no-padding-right">{{trans('label.post.active')}}</label>
            <div class="col-sm-9">
                <div class="gr-act">
                    {!! Form::checkbox('active',1,old('active'),['class'=>'ace ace-switch ace-switch-3','id'=>'pos_active']) !!}
                    <span class="lbl"></span>
                </div>
            </div>
        </div>

    </div>


</div>
<div class="clearfix form-actions">
    <div class="col-md-offset-3 col-md-9">
        <button class="btn btn-info">
            <i class="ace-icon fa fa-check bigger-110"></i>
            {{$submit}}
        </button>
        <button class="btn" type="reset">
            <i class="ace-icon fa fa-undo bigger-110"></i>
            {{trans('label.reset')}}
        </button>
    </div>
</div>
