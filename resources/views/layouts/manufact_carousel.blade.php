<section>
    <div class="carousel margin-30b" id="manuf_carousel">
        <div class="car-item">
            <a href="javascript:;"><img src="/pictures/manuf_vt.png"></a>
        </div>
        <div class="car-item">
            <a href="javascript:;"><img src="/pictures/manuf_mobi.png"></a>
        </div>
        <div class="car-item">
            <a href="javascript:;"><img src="/pictures/manuf_vina.png"></a>
        </div>
        <div class="car-item">
            <a href="javascript:;"><img src="/pictures/manuf_vtv.png"></a>
        </div>
        <div class="car-item">
            <a href="javascript:;"><img src="/pictures/manuf_vtvc.png"></a>
        </div>
        <div class="car-item">
            <a href="javascript:;"><img src="/pictures/manuf_vtc.png"></a>
        </div>
    </div>
    <script type="text/javascript">
        $('#manuf_carousel').slick({
            centerMode: true,
            centerPadding: '0',
            slidesToShow: 6,
            arrows:false,
            responsive: [
            {
              breakpoint: 768,
              settings: {
                arrows: false,
                centerMode: true,
                centerPadding: '0',
                slidesToShow: 4
              }
            },
            {
              breakpoint: 480,
              settings: {
                arrows: false,
                centerMode: true,
                centerPadding: '0',
                slidesToShow: 2
              }
            }
            ]
        })
    </script>
</section>
