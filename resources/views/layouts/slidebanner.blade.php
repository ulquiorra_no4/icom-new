<section class="banner-carousel" id="mid_banner">
    <div class="inner">
        <a href="javascript:;" class="ban-thumb">
            <img src="/pictures/slidebanner1.png" alt="">
        </a>
        @yield('mid')
    </div>
</section>
