<footer id="footer">
	<div class="container">
		<div class="row">
			<div class="col-md-7">
				<div class="bot-logo">
					<a href="/" title="I-com.vn"><img src="/images/logo_1.png" alt="I-com.vn"></a>
				</div>
			</div>
			<div class="col-md-7">
				<div class="info-com">
					<h4>Công ty Cổ phần Dịch vụ Truyền thông Vietnamnet ICOM</h4>
					<p><strong>Địa chỉ :</strong> Tầng 5, Toà nhà HL, ngõ 82 Duy Tân, quận Cầu Giấy, Hà Nội</p>
					<p><strong>Điện thoại   :</strong> (84-24) 37959783</p>
					<p><strong>Email :</strong> support@i-com.vn</p>
				</div>
			</div>
			<div class="col-md-5">

				<div class="bot-social">
					<strong>Connect us on social</strong>
					<ul class="right">
						<li><a href="javascript:;" class="fb"><span class="fa fa-facebook"></span></a></li>
						<li><a href="javascript:;" class="gp"><span class="fa fa-google-plus"></span></a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</footer>
