<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>Công ty CP Dịch vụ Truyền thông VietNamNet Icom</title>
	<link rel="Shortcut Icon" href="/images/favico.ico" type="image/x-icon" />
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"> 
	<link rel="stylesheet" href="{{asset('css/web/styles.min.css')}}">
	<link rel="stylesheet" href="{{asset('css/web/app.css')}}">
	<link rel="stylesheet" href="{{asset('font-awesome/4.5.0/css/font-awesome.min.css')}}">
	<link rel="stylesheet" href="{{asset('css/web/slick.css')}}">
	<script type="text/javascript" src="{{asset('js/web/jquery-2.1.4.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/web/slick.min.js')}}"></script>
</head>
<body>
	@include('layouts.header')
	<main id="main">
		@yield('content')
	</main>
	@include('layouts.footer')
	<script>
		$.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
		function set_lang(key=0){
   			var locale = key ? 'en' : 'vi';
   			$.ajax({
   				url: '{{route('language_set')}}',
   				type: 'POST',
   				dataType: 'json',
   				data:{locale:locale},
   				success:function(resp){},
				error: function(resp){},
				beforeSend:function(){},
				complete:function(resp){
					window.location.reload(true);
				}
   			});
		}
		$('#toggle_menu').on('click',function(){
			$(this).parent().toggleClass('open');
		});
		
	</script>
</body>
</html>
