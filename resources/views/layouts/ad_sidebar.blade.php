<div id="sidebar" class="sidebar responsive ace-save-state">
    <div class="sidebar-shortcuts" id="sidebar-shortcuts">
        <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
            <button class="btn btn-success"><i class="ace-icon fa fa-signal"></i></button>
            <button class="btn btn-info"><i class="ace-icon fa fa-pencil"></i></button>
            <button class="btn btn-warning"><i class="ace-icon fa fa-users"></i></button>
            <button class="btn btn-danger"><i class="ace-icon fa fa-cogs"></i></button>
        </div>
        <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
            <span class="btn btn-success"></span>
            <span class="btn btn-info"></span>
            <span class="btn btn-warning"></span>
            <span class="btn btn-danger"></span>
        </div>
    </div><!-- /.sidebar-shortcuts -->
    <ul class="nav nav-list">
        <li class="active">
            <a href="/admin">
                <i class="menu-icon fa fa-tachometer"></i>
                <span class="menu-text">Dashboard</span>
            </a>
            <b class="arrow"></b>
        </li>
        <li>
            <a href="javascript:;" class="dropdown-toggle">
                <i class="menu-icon fa fa-newspaper-o"></i>
                <span class="menu-text">Posts</span>
                <b class="arrow fa fa-angle-down"></b>
            </a>
            <b class="arrow"></b>
            <ul class="submenu">
                <li>
                    <a href="{{route('pos')}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Listing
                    </a>
                    <b class="arrow"></b>
                </li>
                <li>
                    <a href="{{route('pos_add')}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Add
                    </a>
                    <b class="arrow"></b>
                </li>
            </ul>
        </li>
        <li>
            <a href="javascript:;" class="dropdown-toggle">
                <i class="menu-icon fa fa-arrows-alt"></i>
                <span class="menu-text">Menu</span>
                <b class="arrow fa fa-angle-down"></b>
            </a>
            <b class="arrow"></b>
            <ul class="submenu">
                <li>
                    <a href="{{route('menu')}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Listing
                    </a>
                    <b class="arrow"></b>
                </li>
                <li>
                    <a href="{{route('menu_add')}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Add
                    </a>
                    <b class="arrow"></b>
                </li>
            </ul>
        </li>
        <li>
            <a href="javascript:;" class="dropdown-toggle">
                <i class="menu-icon fa fa-list"></i>
                <span class="menu-text">Categories</span>
                <b class="arrow fa fa-angle-down"></b>
            </a>
            <b class="arrow"></b>
            <ul class="submenu">
                <li>
                    <a href="{{route('cat')}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Listing
                    </a>
                    <b class="arrow"></b>
                </li>
                <li>
                    <a href="{{route('cat_add')}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Add
                    </a>
                    <b class="arrow"></b>
                </li>
            </ul>
        </li>
        <li>
            <a href="javascript:;" class="dropdown-toggle">
                <i class="menu-icon fa fa-cog"></i>
                <span class="menu-text">Config Website</span>
                <b class="arrow fa fa-angle-down"></b>
            </a>
            <b class="arrow"></b>
            <ul class="submenu">
                <li>
                    <a href="{{route('inf')}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Listing
                    </a>
                    <b class="arrow"></b>
                </li>
                <li>
                    <a href="{{route('inf_add')}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Add
                    </a>
                    <b class="arrow"></b>
                </li>
            </ul>
        </li>
        <li>
            <a href="javascript:;" class="dropdown-toggle">
                <i class="menu-icon fa fa-rebel"></i>
                <span class="menu-text">Services</span>
                <b class="arrow fa fa-angle-down"></b>
            </a>
            <b class="arrow"></b>
            <ul class="submenu">
                <li>
                    <a href="{{route('ser')}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Listing
                    </a>
                    <b class="arrow"></b>
                </li>
                <li>
                    <a href="{{route('ser_add')}}">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Add
                    </a>
                    <b class="arrow"></b>
                </li>
            </ul>
        </li>
    </ul><!-- /.nav-list -->
    <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
        <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
    </div>
</div>
