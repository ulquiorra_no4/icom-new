@extends('layouts.app')
@section('mid')
<div class="shadow">
    <div class="container">
        <div class="inner txt-center type2">
            <h2>{{trans('app.news.banner.title')}}</h2>
        </div>
    </div>
</div>
@stop
@section('content')
    <section class="banner-carousel type2 margin-30b" id="mid_banner">
        <div class="inner">
            <a href="javascript:;" class="ban-thumb">
                <img src="/pictures/news_banner.jpg" alt="">
            </a>
            @yield('mid')
        </div>
    </section>
    <section class="mod-news margin-40b">
        <div class="container">
            <div class="row">
                @if($a_data)
                @foreach($a_data as $data)
                <div class="col-md-4 news-item">
                    <a href="{{url_detail('news',$data->slug)}}" title="{{$data->title}}">
                        <img src="{{$data->avatar?'http://i-com.vn/uploads/pictures/news/'.$data->avatar:'http://i-com.vn/uploads/pictures/news/20141125094441_1.jpg'}}" alt="{{$data->title}}">
                        <div class="short-content">
                            <span class="category">{{$data->category['cat_name']}}</span>
                            <h4 class="title">{{$data->title}}</h4>
                        </div>
                        <p><span>{{trans('app.button.more')}}</span></p>
                    </a>
                </div>
                @endforeach
                <div class="col-md-12 text-center">
                    {!! $a_data->render() !!}
                </div>
                @endif
            </div>
        </div>
    </section>
@stop
