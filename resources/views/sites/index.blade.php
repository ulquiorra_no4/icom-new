@extends('layouts.app')

@section('mid')
<div class="shadow">
    <div class="container">
        <div class="inner">
            <h2>{{trans('app.homepage.banner.title')}}</h2>
            <p class="hidden-xs">{{trans('app.homepage.banner.description')}}</p>
            <a href="/about" class="btn-more hidden-xs">{{trans('app.button.more')}}</a>
        </div>
    </div>
</div>
@stop
@section('content')
    @include('layouts.slidebanner')
    <section class="inner-box">
        <div class="module margin-30b" id="intro">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 up-box">
                        <div class="inner-box">
                            <div class="">
                                <strong>{{trans('app.homepage.top.one.title')}}</strong>
                                <span>{{trans('app.homepage.top.one.description')}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 up-box">
                        <div class="inner-box">
                            <div class="">
                                <strong>{{trans('app.homepage.top.two.title')}}</strong>
                                <span>{{trans('app.homepage.top.two.description')}}</span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 up-box">
                        <div class="inner-box">
                            <div class="">
                                <strong>{{trans('app.homepage.top.three.title')}}</strong>
                                <span>{{trans('app.homepage.top.three.description')}}</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 heading-intro border-bot">
                        <h2>{{trans('app.homepage.service.title')}}</h2>
                    </div>
                    <div class="col-md-4 intro-item">
                        <div class="inner-box">
                            <span class="ic ic-ser"></span>
                            <a href="{{url_detail('services',str_slug('DỊCH VỤ GIÁ TRỊ GIA TĂNG'))}}" title="{{trans('app.homepage.service.one.title')}}"><h4>{{trans('app.homepage.service.one.title')}}</h4></a>
                            <p>{{trans('app.homepage.service.one.description')}}</p>
                        </div>
                    </div>

                    <div class="col-md-4 intro-item">
                        <div class="inner-box">
                            <span class="ic ic-pap"></span>
                            <a href="{{url_detail('services',str_slug('NỘI DUNG SỐ'))}}" title="{{trans('app.homepage.service.four.title')}}"><h4>{{trans('app.homepage.service.four.title')}}</h4></a>
                            <p>{{trans('app.homepage.service.four.description')}}</p>
                        </div>
                    </div>
                    <div class="col-md-4 intro-item">
                        <div class="inner-box">
                            <span class="ic ic-pay"></span>
                            <a href="{{url_detail('services',str_slug('GIẢI PHÁP THANH TOÁN'))}}" title="{{trans('app.homepage.service.five.title')}}"><h4>{{trans('app.homepage.service.five.title')}}</h4></a>
                            <p>{{trans('app.homepage.service.five.description')}}</p>
                        </div>
                    </div>
                    <div class="col-md-12 txt-center margin-30b">
                        <a href="/services" class="btn-more cly">{{trans('app.button.more')}}</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 heading-intro border-bot">
                        <h2>{{trans('app.homepage.project.title')}}</h2>
                    </div>
                    <div class="col-md-8 col-md-offset-2 txt-center">
                        <p>{{trans('app.homepage.project.description')}}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="margin-40b">
        <div class="carousel" id="prj_carousel">
            <div class="car-item txt-center">
                <div class="inner" style="background-image:url('/pictures/thumb2.png')">
                    <div class="shadow">
                        <h3>{{trans('app.homepage.project.one.title')}}</h3>
                        <p>{{trans('app.homepage.project.one.description')}}</p>
                        <a href="{{url_detail('services',str_slug('Adsblock'))}}" class="btn-more">{{trans('app.button.more')}}</a>
                    </div>
                </div>
            </div>
            <div class="car-item txt-center">
                <div class="inner" style="background-image:url('/pictures/thumb1.png')">
                    <div class="shadow">
                        <h3>{{trans('app.service.main.two.title')}}</h3>
                        <p>{{trans('app.service.main.two.description')}}</p>
                        <a href="{{url_detail('services',str_slug('PAYTV'))}}" class="btn-more">{{trans('app.button.more')}}</a>
                    </div>
                </div>
            </div>
            <div class="car-item txt-center">
                <div class="inner" style="background-image:url('/pictures/thumb3.png')">
                    <div class="shadow">
                        <h3>{{trans('app.service.main.three.title')}}</h3>
                        <p>{{trans('app.service.main.three.description')}}</p>
                        <a href="{{url_detail('services',str_slug('GIẢI PHÁP MAKETING'))}}" class="btn-more">{{trans('app.button.more')}}</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="heading-intro txt-center margin-30b border-bot">
            <h2>{{trans('app.homepage.customer.title')}}</h2>
            <p>{{trans('app.homepage.customer.description')}}</p>
        </div>
    </section>
    @include('layouts.manufact_carousel')
    <script type="text/javascript">
    // $('#prj_carousel').slick({
    //     centerMode: true,
    //     centerPadding: '0',
    //     slidesToShow: 3,
    //     arrows:false,
    //     responsive: [
    //     {
    //       breakpoint: 768,
    //       settings: {
    //         arrows: false,
    //         centerMode: true,
    //         centerPadding: '0',
    //         slidesToShow: 2
    //       }
    //     },
    //     {
    //       breakpoint: 480,
    //       settings: {
    //         arrows: false,
    //         centerMode: true,
    //         centerPadding: '0',
    //         slidesToShow: 1
    //       }
    //     }
    //     ]
    // });
    </script>
@stop
