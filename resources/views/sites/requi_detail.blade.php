@extends('layouts.app')
@section('mid')
<div class="shadow">
    <div class="container">
        <div class="inner txt-center">
            <h3>Tuyển dụng</h3>
            <ul class="abt hidden-xs">
                <li>Đam mê</li>
                <li>Tận tâm</li>
                <li>Phát triển</li>
            </ul>
        </div>
    </div>
</div>
@stop
@section('content')
<section class="banner-carousel" id="mid_banner">
    <div class="inner">
        <a href="/">
            <img src="/pictures/slidebanner2.png" alt="">
        </a>
        @yield('mid')
    </div>
</section>
<section class="mod-jod-detail">
    <div class="container">
        <div class="title">
            <h4>{{$detail->title}}</h4>
        </div>
        <article class="short-detail margin-30b">
            <div class="row">
                <div class="col-md-6">
                    <p><strong>Địa điểm </strong> {{$detail->place?$detail->place:'Hà Nội'}}</p>
                    <p><strong>Chức danh </strong> {{$detail->position?$detail->position:'Nhân viên'}}</p>
                </div>
                <div class="col-md-6">
                    <p><strong>Mức lương </strong> {{$detail->salary?$detail->salary:'Thỏa thuận'}}</p>
                </div>
            </div>
        </article>
        <article class="detail">
            <div class="inner">
                <h4>Mô tả công việc:</h4>
                {!! $detail->content !!}
            </div>
        </article>
    </div>
</section>
@stop
