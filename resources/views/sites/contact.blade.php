@extends('layouts.app')
@section('mid')
<div class="shadow">
    <div class="container">
        <div class="inner">
            <div class="txt-center txt-wc">
                <h3>{{trans('app.contact.banner.title')}}</h3>
                <h4>{{trans('app.contact.banner.description')}}</h4>
            </div>
        </div>
    </div>
</div>
@stop
@section('content')
    <section class="banner-carousel" id="mid_banner">
        <div class="inner">
            <a href="javascript:;">
                <img src="/pictures/slidebanner3.png" alt="">
            </a>
            @yield('mid')
        </div>
    </section>
    <section class="margin-30b">
        <div class="container">
            <div class="row">
                <div class="col-md-12 heading-intro txt-center border-bot">
                    <h2>{{trans('app.contact.form.title')}}</h2>
                </div>
                <form action="" method="post" id="frm_contact">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="ct_name">{{trans('app.contact.form.label.name')}} <span>*</span></label>
                            <input type="text" name="ct_name" id="ct_name" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="ct_phone">{{trans('app.contact.form.label.phone')}} <span>*</span></label>
                            <input type="text" name="ct_phone" id="ct_phone" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="ct_mail">{{trans('app.contact.form.label.email')}} <span>*</span></label>
                            <input type="email" name="ct_mail" id="ct_mail" class="form-control" required>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="ct_company">{{trans('app.contact.form.label.company')}} </label>
                            <input type="text" name="ct_company" id="ct_company" class="form-control">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="ct_mess">{{trans('app.contact.form.label.message')}} <span>*</span></label>
                            <textarea name="ct_mess" rows="8" id="ct_mess" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="col-md-12 txt-center">
                        <button class="btn btn-send">{{trans('app.button.send')}}</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
@stop
