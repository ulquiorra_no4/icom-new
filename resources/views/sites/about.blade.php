@extends('layouts.app')
@section('mid')
<div class="shadow">
    <div class="container">
        <div class="inner txt-center">
            <h3>{{trans('app.about.banner.title')}}</h3>
            <ul class="abt  hidden-xs">
                <li>{{trans('app.about.banner.one.title')}}</li>
                <li>{{trans('app.about.banner.two.title')}}</li>
                <li>{{trans('app.about.banner.three.title')}}</li>
            </ul>
        </div>
    </div>
</div>
@stop
@section('content')
    <section class="banner-carousel type2" id="mid_banner">
        <div class="inner">
            <a href="javascript:;" class="ban-thumb">
                <img src="/pictures/slidebanner1.png" alt="">
            </a>
            @yield('mid')
        </div>
    </section>
    <section class="about-info margin-20b bg-e1">
        <div class="container">
            <div class="col-md-12 txt-center">
                <div class="heading">
                    <h2>{{trans('app.about.top.title')}}</h2>
                </div>
                <p>{{trans('app.about.top.description')}}</p>
            </div>
        </div>
    </section>
    <section class="margin-30b">
        <div class="container">
            <div class="col-md-12 txt-center">
                <div class="heading-intro border-bot">
                    <h2>{{trans('app.about.mid.title')}}</h2>
                    <p>{{trans('app.about.mid.description')}}</p>
                </div>
            </div>
        </div>
    </section>
    <section class="margin-30b">
        <div class="container">
            <div class="col-md-12 txt-center">
                <div class="heading-intro border-bot">
                    <h2>{{trans('app.about.value.title')}}</h2>
                </div>
                <div class="col-md-6 core-item core1">
                    <h3>{{trans('app.about.value.one.title')}}</h3>
                    <p>{{trans('app.about.value.one.description')}}</p>
                </div>
                <div class="col-md-6 core-item core2">
                    <h3>{{trans('app.about.value.two.title')}}</h3>
                    <p>{{trans('app.about.value.two.description')}}</p>
                </div>
                <div class="col-md-6 core-item core3">
                    <h3>{{trans('app.about.value.three.title')}}</h3>
                    <p>{{trans('app.about.value.three.description')}}</p>
                </div>
                <div class="col-md-6 core-item core4">
                    <h3>{{trans('app.about.value.four.title')}}</h3>
                    <p>{{trans('app.about.value.four.description')}}</p>
                </div>
            </div>
        </div>
    </section>
    <section class="margin-30b relative">
        <div class="gallery-box">
            <figure class="item">
                <img src="/pictures/ours1.png" alt="">
            </figure>
            <figure class="item">
                <img src="/pictures/ours2.png" alt="">
            </figure>
            <figure class="item">
                <img src="/pictures/ours3.png" alt="">
            </figure>
            <figure class="item">
                <img src="/pictures/ours4.png" alt="">
            </figure>
            <figure class="item">
                <img src="/pictures/ours5.png" alt="">
            </figure>
            <figure class="item">
                <img src="/pictures/ours6.png" alt="">
            </figure>
            <figure class="item">
                <img src="/pictures/ours7.png" alt="">
            </figure>
            <figure class="item">
                <img src="/pictures/ours8.png" alt="">
            </figure>
            <div class="shadow">
                <div class="inner txt-center">
                    <h2>{{trans('app.about.human.title')}}</h2>
                    <p><span class="fa fa-quote-left"></span>{{trans('app.about.human.description')}}<span class="fa fa-quote-right
                        "></span></p>
                </div>
            </div>
        </div>

    </section>
    <section>
        <div class="container">
            <div class="heading-intro txt-center margin-30b">
                <h2>{{trans('app.about.customer.title')}}</h2>
                <p>{{trans('app.about.customer.description')}}</p>
            </div>
        </div>
    </section>
    @include('layouts.manufact_carousel')
@stop
