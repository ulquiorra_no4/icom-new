@extends('layouts.app')
@section('mid')
<div class="shadow">
    <div class="container">
        <div class="inner txt-center">
            <h3>{{trans('app.service.banner.title')}}</h3>
            <ul class="abt">
                <li>{{trans('app.service.banner.one.title')}}</li>
                <li>{{trans('app.service.banner.two.title')}}</li>
                <li>{{trans('app.service.banner.three.title')}}</li>
            </ul>
        </div>
    </div>
</div>
@stop
@section('content')
    <section class="banner-carousel type2" id="mid_banner">
        <div class="inner">
            <a href="javascript:;" class="ban-thumb">
                <img src="/pictures/slidebanner1.png" alt="">
            </a>
            @yield('mid')
        </div>
    </section>
    <section class="padding-box margin-20b">
        <div class="container">
            <div class="col-md-12 txt-center">
                <div class="heading-intro border-bot">
                    <h2>{{trans('app.service.top.title')}}</h2>
                </div>
                <p>{{trans('app.service.top.description')}}</p>
            </div>
        </div>
    </section>
    <section class="margin-30b" id="servicer_box">
        <article>
            <div class="col50">
                <div class="inner-info">
                    <h3>{{trans('app.service.main.one.title')}}</h3>
                    <p>{{trans('app.service.main.one.description')}}</p>
                    <a href="{{url_detail('services',str_slug('DỊCH VỤ GIÁ TRỊ GIA TĂNG'))}}" class="btn-more cly">{{trans('app.button.more')}}</a>
                </div>
            </div>
            <div class="col50">
                <figure>
                    <img src="/pictures/vat.jpg" alt="">
                </figure>
            </div>
        </article>
        <article>
            <div class="col50">
                <figure>
                    <img src="/pictures/pay.jpg" alt="">
                </figure>
            </div>
            <div class="col50">
                <div class="inner-info">
                    <h3>{{trans('app.service.main.four.title')}}</h3>
                    <p>{{trans('app.service.main.four.description')}}</p>
                    <a href="{{url_detail('services',str_slug('GIẢI PHÁP THANH TOÁN'))}}" class="btn-more cly">{{trans('app.button.more')}}</a>
                </div>
            </div>
        </article>
        <article>
            <div class="col50">
                <div class="inner-info">
                    <h3>{{trans('app.service.main.five.title')}}</h3>
                    <p>{{trans('app.service.main.five.description')}}</p>
                    <a href="{{url_detail('services',str_slug('NỘI DUNG SỐ'))}}" class="btn-more cly">{{trans('app.button.more')}}</a>
                </div>
            </div>
            <div class="col50">
                <figure>
                    <img src="/pictures/digital_content.jpg" alt="">
                </figure>
            </div>
        </article>
    </section>
    <section class="padding-box bg-e1">
        <div class="container">
            <div class="col-md-12 txt-center">
                <div class="heading-intro border-bot">
                    <h2>{{trans('app.service.contact.title')}}</h2>
                </div>
                <p>{{trans('app.service.contact.description')}}</p>
                <a href="/contact" class="btn-more cly">{{trans('app.button.contact')}}</a>
            </div>
        </div>
    </section>
@stop
