@extends('layouts.app')
@section('mid')
<div class="shadow">
    <div class="container">
        <div class="inner">
            <div class="right mid-ser-dt">
                <h2></h2>
            </div>
        </div>
    </div>
</div>
@stop
@section('content')
    <section class="banner-carousel" id="mid_banner">
        <div class="inner">
            <a href="javascript:;" class="ban-thumb">
                <img src="{{$detail['banner_thumb']}}" alt="{{$detail['title']}}">
            </a>
        </div>
    </section>
    <section class="padding-box margin-20b">
        <div class="container">
            <div class="col-md-12 txt-center">
                <div class="heading-intro border-bot">
                    <h1>{{$detail['title']}}</h1>
                </div>
                <p>{{$detail['description']}}</p>
            </div>
        </div>
    </section>
    <section class="margin-30b" id="ser_info">
        <article class="" style="background:url('{{$detail['thumbnail']}}') no-repeat center;min-height:500px">
            @if($detail['description_2'])
            <div class="right">
                <p><span class="fa fa-quote-left"></span>{{$detail['description_2']}}<span class="fa fa-quote-right
                    "></span></p>
            </div>
            @endif
        </article>
    </section>
    @if($detail['value']==1)
    <section class="margin-30b">
        <div class="container">
            <div class="heading-intro txt-center">
                <h2>{{$detail['content']['title']}}</h2>
            </div>
            <div class="row">
                <ul class="ser-child">
                    <li class="col-md-3">
                        <div class="inner">
                            <div class="head">
                                <h4>{{$detail['content']['one']['title']}}</h4>
                                <span class="icon icon-call"></span>
                            </div>
                            <p>{{$detail['content']['one']['description']}}</p>
                            <i class="fa fa-angle-down"></i>
                        </div>
                    </li>
                    <li class="col-md-3">
                        <div class="inner">
                            <div class="head">
                                <h4>{{$detail['content']['two']['title']}}</h4>
                                <span class="icon-radio"></span>
                            </div>
                            <p>{{$detail['content']['two']['description']}}</p>
                            <i class="fa fa-angle-down"></i>
                        </div>
                    </li>
                    <li class="col-md-3">
                        <div class="inner">
                            <div class="head">
                                <h4>{{$detail['content']['three']['title']}}</h4>
                                <span class="icon icon-book"></span>
                            </div>
                            <p>{{$detail['content']['three']['description']}}</p>
                            <i class="fa fa-angle-down"></i>
                        </div>
                    </li>
                    <li class="col-md-3">
                        <div class="inner">
                            <div class="head">
                                <h4>{{$detail['content']['four']['title']}}</h4>
                                <span class="icon icon-care"></span>
                            </div>
                            <p>{{$detail['content']['four']['description']}}</p>
                            <i class="fa fa-angle-down"></i>
                        </div>
                    </li>
                </ul>

            </div>
        </div>
    </section>
    @else
    <section class="margin-30b">
        <div class="container">
            <article>
                {!! $detail['content'] !!}
            </article>
        </div>
    </section>
    @endif
    <section class="bg-e1 padding-box">
        <div class="container">
            <div class="col-md-12 txt-center">
                <div class="heading-intro border-bot">
                    <h2>LIÊN HỆ VỚI CHÚNG TÔI</h2>
                </div>
                <p>Để tìm hiểu thêm về dịch vụ</p>
                <a href="/contact" class="btn-more cly">{{trans('app.button.contact')}}</a>
            </div>
        </div>
    </section>
    <script type="text/javascript">
        $('.ser-child li').find('i').on('click',function(){
            if($(this).hasClass('fa-angle-down')){
                $(this).parent().find('p').show(300);
                $(this).removeClass('fa-angle-down');
                $(this).addClass('fa-angle-up');
            }else{
                $(this).parent().find('p').hide(400);
                $(this).removeClass('fa-angle-up');
                $(this).addClass('fa-angle-down');
            }
        })
    </script>
@stop
