@extends('layouts.app')
@section('mid')
<div class="shadow">
    <div class="container">
        <div class="inner txt-center">
            <h3>Tuyển dụng</h3>
            <ul class="abt hidden-xs">
                <li>Đam mê</li>
                <li>Tận tâm</li>
                <li>Phát triển</li>
            </ul>
        </div>
    </div>
</div>
@stop
@section('content')
    <section class="banner-carousel" id="mid_banner">
        <div class="inner">
            <a href="/">
                <img src="/pictures/slidebanner2.png" alt="">
            </a>
            @yield('mid')
        </div>
    </section>
    <section class="margin-40b">
        <div class="container">
            <div class="heading margin-30b">
                <h2>DANH SÁCH TUYỂN DỤNG</h2>
            </div>
            <div class="find-box">
                <form class="margin-30b">
                    <div class="col-80">
                        <input type="text" name="job" placeholder="Vị trí ứng tuyển" value="{{$keyword}}">
                    </div>
                    <div class="col-20">
                        <button class="btn btn-find">Tìm kiếm</button>
                    </div>
                </form>
            </div>
            <div class="margin-50b">

            </div>
            <div class="module-job">
                @if($a_data)
                <table class="table table-bordered table-hover">
                    <thead style="background:#959595;color:#fff">
                        <tr>
                            <th style="text-align:center">TT</th>
                            <th style="text-align:center">Ngày đăng</th>
                            <th style="text-align:left">Công việc</th>
                            <th style="text-align:center">Địa điểm</th>
                            <th style="text-align:center">Mức lương</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($a_data as $k => $data)
                        <tr style="color:#000;text-align:center">
                            <td>{{$k+1}}</td>
                            <td>{{$data->published_at}}</td>
                            <td style="font-weight:600;text-align:left"><a href="{{url_detail('requirement',$data->slug)}}" title="{{$data->title}}">{{$data['title']}}</a></td>
                            <td>{{$data->place?$data->place:'Hà Nội'}}</td>
                            <td>{{$data->salary?$data->salary:'Thỏa thuận'}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                {!! $a_data->render() !!}
                @endif
            </div>
            <div class="clear"></div>
        </div>
    </section>
@stop
