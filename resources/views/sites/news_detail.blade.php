@extends('layouts.app')
@section('mid')
<div class="shadow">
    <div class="container">
        <div class="inner txt-center type2">
            <h2>{{trans('app.news.banner.title')}}</h2>
        </div>
    </div>
</div>
@stop
@section('content')
    <section class="banner-carousel type2 margin-30b" id="mid_banner">
        <div class="inner">
            <a href="javascript:;" class="ban-thumb">
                <img src="/pictures/news_banner.jpg" alt="">
            </a>
            @yield('mid')
        </div>
    </section>
    <section class="mod-news margin-40b">
        <div class="container">
            <div class="row">
                <aside class="col-md-4">
                    <div class="thumb">
                        <img src="{{$detail->avatar?'http://i-com.vn/uploads/pictures/news/'.$detail->avatar:'http://i-com.vn/uploads/pictures/news/20141125094441_1.jpg'}}" alt="{{$detail->title}}">
                    </div>
                    <div class="">
                        <div class="heading">
                            <h2>Bài viết Mới nhất</h2>
                        </div>
                        @foreach($latest as $news)
                        <div class="news-item">
                            <a href="{{url_detail('news',$news->slug)}}" title="{{$news->title}}">
                                <img src="{{$news->avatar?'http://i-com.vn/uploads/pictures/news/'.$news->avatar:'http://i-com.vn/uploads/pictures/news/20141125094441_1.jpg'}}" alt="{{$news->title}}">
                                <div class="short-content">
                                    <span class="category">Sự kiện</span>
                                    <h4 class="title">{{$news->title}}</h4>
                                </div>
                                <p><span>{{trans('app.button.more')}}</span></p>
                            </a>
                        </div>
                        @endforeach
                    </div>
                </aside>
                <article class="col-md-8 detail-content">
                    <h1 class="title">{{$detail->title}}</h1>
                    <p><span style="margin-right:20px">Sự kiện</span><span>{{$detail->published_at}}</span></p>
                    <div class="inner">
                        {!! $detail->content !!}
                    </div>
                    <div class="inner">
                        @if($relation)
                        <h2>Bài viết liên quan</h2>
                        <ul class="rel">
                            @foreach($relation as $news)
                            <li><a href="{{url_detail('news',$news->slug)}}" title="{{$news->title}}">{{$news->title}}</a></li>
                            @endforeach
                        </ul>
                        @endif
                    </div>
                </article>
            </div>
        </div>
    </section>
@stop
