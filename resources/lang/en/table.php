<?php
/**
 * Created by PhpStorm.
 * User: HiepHM
 * Date: 11/23/2017
 * Time: 8:32 AM
 */
return [
    'module' => [
        'name' => 'Listing modules'
    ],
    'category'=>[
        'name'=>'Listing categories'
    ],
    'post' => 'Listing posts',
    'menu'=>[
        'name'=>'Menu'
    ],
    'infosite'=>[
        'name'=>'Infomation website'
    ],
    'service'=>[
        'name'=>'Service'
    ]
];
