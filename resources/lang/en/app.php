<?php


return [
    'menu'=>[
        'home'=>'Home',
        'us'=>'About us',
        'service'=>'Services',
        'news'=>'News',
        'requi'=>'Work with us',
        'contact'=>'Contact'
    ],
    'homepage'=>[
    	'banner'=>[
    		'title'=>'10+ years of excellence delivering value-added services',
    		'description'=>'ICOM is a pioneer in developing and providing VAS services and a strategic partner of major telecommunications companies in Vietnam.'
    	],
    	'top'=>[
    		'one'=>[
    			'title'=>'100+',
    			'description'=>'Successful projects'
    		],
    		'two'=>[
    			'title'=>'TOP 3',
    			'description'=>'VAS provider'
    		],
    		'three'=>[
    			'title'=>'5,000,000+',
    			'description'=>'Customers in Vietnam'
    		]
    	],
    	'service'=>[
    		'title'=>'OUR MAIN SERVICES',
    		'description'=>'',
    		'one'=>[
    			'title'=>'Value-added services',
    			'description'=>'We partner with biggest mobile operators in Vietnam to provide hundreds of Value added services to millions of mobile users in Vietnam'
    		],
    		'two'=>[
    			'title'=>'PayTV',
    			'description'=>'We provide a multi-plaform interactive television service on web, mobile web and mobile application.'
    		],
    		'three'=>[
    			'title'=>'Marketing Solution',
    			'description'=>'We are a strategic partner of Google in Vietnam to develop online advertising and big data services.'
    		],
    		'four'=>[
    			'title'=>'Digital Content',
    			'description'=>'Our e-newspapers become popular hubs for millions of Vietnamese readers and serve millions of impressions per day'
    		],
    		'five'=>[
    			'title'=>'Payment Solution',
    			'description'=>'We focus on developing payment and finance solution as well as providing e-wallet to mobile users'
    		],
    	],
    	'project'=>[
    		'title'=>'Discover our new projects',
    		'description'=>'We are focused on developing new projects which keep updated with leading-edge technology that is ever-evolving.',
    		'one'=>[
    			'title'=>'AdBlock Solution',
    			'description'=>'Protect mobile users from annoying advertisement, phising websites and harmful content when using Internet through 3G/4G'
    		],
    		'two'=>[
    			'title'=>'Social Listening Solution',
    			'description'=>'Identify influencers, discover new social audience segments, and analyze emerging conversations related to your brand to drive brand awareness'
    		],
    		'three'=>[
    			'title'=>'Social Care Solution',
    			'description'=>'Help companies to manage their customers easier and more flexibly'
    		]
    	],
    	'customer'=>[
    		'title'=>'Customer & Network',
    		'description'=>'We work with partners all over the world'
    	]
    ],
    'about'=>[
    	'banner'=>[
    		'title'=>'About us',
    		'description'=>'',
    		'one'=>[
    			'title'=>'BUSINESS',
    			'description'=>''
    		],
    		'two'=>[
    			'title'=>'TECHNOLOGY',
    			'description'=>''
    		],
    		'three'=>[
    			'title'=>'SUCCESS',
    			'description'=>''
    		],

    	],
    	'top'=>[
    		'title'=>'10+ YEARS OF EXCELLENCE DELIVERING VALUE-ADDED SERVICES',
    		'description'=>'Founded in 2008, ICOM is a leading company in providing solutions for Digital content, Value added services and communication inVietnam.

Currently, ICOM is the No.1 partner with biggest Telcos in Vietnam such as MobiFone, VinaPhone, Gmoblie, Viettel,Vietnammobile, etc.'
        ],
        'mid'=>[
            'title'=>'Vision & Mission',
            'description'=>'Our mission is to provide customers with best value, especially Telecom products and value added services.

Customer satisfaction is our ultimate goal and guideline in our business.'
        ],
        'human'=>[
            'title'=>'Human resources',
            'description'=>'At ICOM, we always put our people first. Our employees are our first priority and we attract and retain our talents by fostering succession planning and career paths that will provide bigger and better advancement opportunities. We listen to our employees’ voice so that together we can all say how proud we are to work at ICOM.'
        ],
        'customer'=>[
            'title'=>'Customer & Network',
            'description'=>'ICOM is focused on delivering services to our public sector, mid-market and large enterprise clients in various industries including healthcare, media and entertainment, retail, manufacturing, education, government and professional services.'
        ],
        'value'=>[
            'title'=>'Core values',
            'description'=>'',
            'one'=>[
                'title'=>'Creativity',
                'description'=>'We thrive to create the best results'
            ],
            'two'=>[
                'title'=>'Integrity',
                'description'=>'We strive to do what’s right for our customers, our teammates and our partners'
            ],
            'three'=>[
                'title'=>'Enthusiasm',
                'description'=>'We devote all of our energy for our work and company activities'
            ],
            'four'=>[
                'title'=>'Innovation',
                'description'=>'We innovate in a highly pragmatic way'
            ],
        ]
    ],
    'service'=>[
    	'banner'=>[
    		'title'=>'Our services',
    		'description'=>'',
    		'one'=>[
    			'title'=>'Quality',
    			'description'=>''
    		],
    		'two'=>[
    			'title'=>'Diversity',
    			'description'=>''
    		],
    		'three'=>[
    			'title'=>'Creativity',
    			'description'=>''
    		],
    	],
    	'top'=>[
    		'title'=>'Take customer experience to the next level',
    		'description'=>'With the explosion of smartphones and 3G/4G, customers are always looking for new, exciting services using modern technologies. By leveraging years of experience, ICOM has been modernizing and revolutionizing our serivce by bringing next-generation technologies and expertise that allow us to create integrated customer experiences that set our business apart.'
    	],
    	'main'=>[
    		'title'=>'',
    		'description'=>'',
    		'one'=>[
    			'title'=>'Value-added services',
    			'description'=>'Là một trong những đơn vị đi đầu trong lĩnh vực giá trị gia tăng, trong suốt hơn 10 năm qua, chúng tôi là đối tác chiến lược của các công ty viễn thông di động như Viettel, MobiFone, Vinaphone... cung cấp hàng trăm dịch vụ giá trị gia tăng cho hàng triệu người dùng tại Việt Nam'
    		],
    		'two'=>[
    			'title'=>'PayTV',
    			'description'=>'Provide subscribers access to hundreds of TV channels and shows bundled with data packages to watch anytime, anywhere'
    		],
    		'three'=>[
    			'title'=>'Marketing Solution',
    			'description'=>'Improve marketing performance of brands and enterprises with marketing solutions cooperated with Google'
    		],
    		'five'=>[
    			'title'=>'Digital Content',
    			'description'=>'Emdep và Myidol là hai trang báo điện tử hàng đầu về phụ nữ và giải trí tại Việt Nam, được ICOM sở hữu và vận hành, phục vụ hàng triệu độc giả ở Việt Nam'
    		],
    		'four'=>[
    			'title'=>'Payment Solution',
    			'description'=>'ICOM là nhà phát triển các giải pháp thanh toán giúp cải thiện trải nghiệm người dùng. Thông qua các giải pháp này, thuê bao di động có thể ứng trước tiền, cuộc gọi/tin nhắn SMS/3G hoặc 4G để sử dụng.'
    		],
    	],
    	'contact'=>[
    		'title'=>'Contact us',
    		'description'=>'Hãy liên hệ với chúng tôi để tìm hiểu & hợp tác phát triển các dịch vụ'
    	]
    ],
    'contact'=>[
    	'banner'=>[
    		'title'=>'Hello!',
    		'description'=>'Send us a message'
    	],
    	'form'=>[
    		'title'=>'Contact us',
    		'label'=>[
    			'name'=>'Full Name',
    			'phone'=>'Phone Number',
    			'email'=>'Business Email',
    			'company'=>'Comany Name',
    			'message'=>'What are you interested in?'
    		]
    	]
    ],
    'news'=>[
        'banner'=>[
            'title'=>'Icom News',
            'description'=>''
        ]
    ],
    'button'=>[
    	'more'=>'View more',
    	'contact'=>'Contact us',
    	'send'=>'Send'
    ]
];
