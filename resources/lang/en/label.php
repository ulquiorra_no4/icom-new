<?php
/**
 * Created by PhpStorm.
 * User: HiepHM
 * Date: 11/17/2017
 * Time: 11:12 AM
 */
return [
    'add' => 'Add',
    'update' => 'Update',
    'reset' => 'Reset',
    'note' => [
        'required' => 'The fields must be not empty'
    ],
    'created_at' => 'Created',
    'updated_at' => 'Updated',
    'module' => [
        'code' => 'Module Code',
        'name' => 'Module Name',
        'nameEn' => 'English Name',
        'alias' => 'Alias',
        'description' => 'Description',
        'active' => 'Active',
        'order' => 'Order',
    ],
    'category' => [
        'name' => 'Category Name',
        'nameEn' => 'English Name',
        'alias' => 'Alias',
        'description' => 'Description',
        'active' => 'Active',
        'order' => 'Order',
        'catParent' => 'Category Parent'
    ],
    'post'=>[
        'title' => 'Post Title',
        'slug'=> 'Post Slug',
        'description'=>'Description',
        'content' =>'Content',
        'keywords' => 'Keywords',
        'catId'=> 'Category',
        'catTag'=>'Category Tags',
        'tag' => 'Tags',
        'avatar' => 'Avatar',
        'stage' => 'State',
        'active' => 'Active',
        'activeTime' => 'Active Time',
        'option' => 'Option',
        'source' => 'Source',
        'sourceUrl' => 'Source Url',
        'partner' => 'Partner',
        'publish' => 'Published'
    ],
    'menu'=>[
        'name'=>'Name',
        'nameEn'=>'English Name',
        'alias'=>'Alias',
        'link'=>'Link',
        'active'=>'Active',
        'icon'=>'Icon',
        'order'=>'Order'
    ],
    'infosite'=>[
        'title'=>'Title',
        'titleEn'=>'English Title',
        'description'=>'Description',
        'descriptionEn'=>'En Description',
        'active'=>'Active',
        'order'=>'Order',
        'page'=>'On Page',
        'position'=>'Position',
        'sub'=>'Is Sub'
    ],
    'service'=>[
        'title'=>'Title',
        'titleEn'=>'English Title',
        'description'=>'Description',
        'descriptionEn'=>'En Description',
        'quote'=>'Quote',
        'quoteEn'=>'En Quote',
        'content'=>'Content',
        'contentEn'=>'En Content',
        'avatar'=>'Avatar',
        'image'=>'Image thumbnail',
        'active'=>'Active',
        'hot'=>'Hot',
        'home'=>'Set home',
        'order'=>'Order'
    ]
];
