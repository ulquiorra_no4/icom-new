<?php
/**
 * Created by PhpStorm.
 * User: HiepHM
 * Date: 11/20/2017
 * Time: 9:11 AM
 */
return [
    'title'=>[
        'delete'=>'Are you sure delete this item ?',
        'multiDel'=>'Are you sure delete item selected ?'
    ],
    'success' => [
        'add' => 'Created success.',
        'update' => 'Updated success.',
        'delete' => 'You have been deleted'
    ],
    'error' => [
        'delete' => 'Delete fail !'
    ]
];