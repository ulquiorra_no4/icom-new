<?php
/**
 * Created by PhpStorm.
 * User: Ulquiorra
 * Date: 8/31/2017
 * Time: 4:05 PM
 */
return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Tài khoản đăng nhập không đúng!',
    'throttle' => 'Hệ thống đang xử lý. Vui lòng thử lại sau :seconds giây.',

];