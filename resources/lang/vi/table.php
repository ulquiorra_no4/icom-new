<?php
/**
 * Created by PhpStorm.
 * User: HiepHM
 * Date: 11/23/2017
 * Time: 8:33 AM
 */
return [
    'module' => [
        'name' => 'Danh sách module'
    ],
    'category'=>[
        'name'=>'Danh sách danh mục'
    ],
    'post' => 'Bài viết',
    'menu'=>[
        'name'=>'Menu'
    ],
    'infosite'=>[
        'name'=>'Thông tin website'
    ],
    'service'=>[
        'name'=>'Dịch vụ'
    ]
];
