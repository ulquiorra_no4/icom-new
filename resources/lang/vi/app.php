<?php


return [
    'menu'=>[
        'home'=>'Trang chủ',
        'us'=>'Về chúng tôi',
        'service'=>'Dịch vụ',
        'news'=>'Tin tức',
        'requi'=>'Tuyển dụng',
        'contact'=>'Liên hệ'
    ],
    'homepage'=>[
    	'banner'=>[
    		'title'=>'10+ năm kinh nghiệm cung cấp Dịch vụ giá trị gia tăng',
    		'description'=>'ICOM là đơn vị tiên phong trong lĩnh vực phát triển và cung cấp dịch vụ GTGT và đối tác chiến lược của các công ty viễn thông lớn ở Việt Nam'
    	],
    	'top'=>[
    		'one'=>[
    			'title'=>'100+',
    			'description'=>'Dự án thành công'
    		],
    		'two'=>[
    			'title'=>'TOP 3',
    			'description'=>'Công ty cung cấp dịch vụ GTGT'
    		],
    		'three'=>[
    			'title'=>'5,000,000+',
    			'description'=>'Người dùng ở Việt Nam'
    		]
    	],
    	'service'=>[
    		'title'=>'DỊCH VỤ CỦA CHÚNG TÔI',
    		'description'=>'',
    		'one'=>[
    			'title'=>'Dịch vụ Giá trị gia tăng',
    			'description'=>'ICOM cung cấp hàng trăm dịch vụ giá trị gia tăng hữu ích tới hàng triệu thuê bao di động ở Việt Nam'
    		],
    		'two'=>[
    			'title'=>'PayTV',
    			'description'=>'ICOM cung cấp dịch vụ Tương tác Truyền hình trên đa nền tảng với nội dung đặc sắc và có bản quyền'
    		],
    		'three'=>[
    			'title'=>'Giải pháp Marketing',
    			'description'=>'ICOM là đối tác chiến lược của Google ở thị trường Việt Nam cùng hợp tác và phát triển online advertising và Big Data'
    		],
    		'four'=>[
    			'title'=>'Nội dung số',
    			'description'=>'ICOM sở hữu và vận hành hai trang báo điện tử, phục vụ hàng triệu độc giả ở Việt Nam'
    		],
    		'five'=>[
    			'title'=>'Giải pháp thanh toán',
    			'description'=>'ICOM phát triển và cung cấp giải pháp thanh toán & tài chính và ví điện tử'
    		],
    	],
    	'project'=>[
    		'title'=>'DỰ ÁN MỚI',
    		'description'=>'Chúng tôi phát triển nhiều dự án ứng dụng những công nghệ hiện đại trên thế giới',
    		'one'=>[
    			'title'=>'Giải pháp Chặn quảng cáo',
    			'description'=>'Giải pháp giúp loại bỏ quảng cáo, cảnh báo website giả mạo, dảm bảo an toàn cho thuê bao di động khi truy cập các website trên Internet thông qua 3G/4G.'
    		],
    		'two'=>[
    			'title'=>'Giải pháp Social Listening',
    			'description'=>'Giải pháp giúp xác định những người có ảnh hưởng và phân tích các chủ đề nổi bật liên quan đến thương hiệu của doanh nghiệp'
    		],
    		'three'=>[
    			'title'=>'Giải pháp Social Care',
    			'description'=>'Giải pháp giúp doanh nghiệp quản lý khách hàng, phát hiện phân khúc khách hàng mới dễ dàng và hiệu qủa hơn'
    		]
    	],
    	'customer'=>[
    		'title'=>'KHÁCH HÀNG & ĐỐI TÁC',
    		'description'=>'Chúng tôi có mạng lưới khách hàng và đối tác trên toàn thế giới'
    	]
    ],
    'about'=>[
    	'banner'=>[
    		'title'=>'Về chúng tôi',
    		'description'=>'',
    		'one'=>[
    			'title'=>'Kinh doanh',
    			'description'=>''
    		],
    		'two'=>[
    			'title'=>'Công nghệ',
    			'description'=>''
    		],
    		'three'=>[
    			'title'=>'Thành công',
    			'description'=>''
    		],

    	],
    	'top'=>[
    		'title'=>'Trên 10 năm kinh nghiệm trong lĩnh vực kinh doanh nội dung số',
    		'description'=>'Được thành lập năm 2008, ICOM là một trong những công ty dẫn đầu trong lĩnh vực cung cấp các dịch vụ Giá trị gia tăng, nội dung số và truyền thông ở Việt Nam.

Hiện nay, ICOM là đối tác chiến lược với các công ty viễn thông lớn nhất tại Việt Nam như MobiFone, VinaPhone, Gmoblie, Viettel, Vietnammobile'
        ],
        'mid'=>[
            'title'=>'Tầm nhìn & Sứ mệnh',
            'description'=>'Sứ mệnh của chúng tôi là cung cấp tới khách hàng những giá trị tốt nhất thông qua các sản phẩm viễn thông và dịch vụ giá trị gia tăng
Sự hài của khách hàng là tôn chỉ và “kim chỉ nam” trong hoạt động kinh doanh của chúng tôi.'
        ],
        'human'=>[
            'title'=>'Cơ cấu nhân sự',
            'description'=>'Chúng tôi xác định nhân sự là tài sản quý giá nhất của doanh nghiệp. Do đó, chúng tôi cam kết đầu tư lâu dài để phát triển chất lượng nhân sự của toàn Công ty. Chúng tôi luôn hoan nghênh những tài năng có chung chí hướng để cùng nhau xây dựng một tập thể hùng mạnh.'
        ],
        'customer'=>[
            'title'=>'Đối tác & Khách hàng',
            'description'=>'Mạng lưới đối tác và khách hàng của ICOM bao phủ từ các bộ ban ngành, những doanh nghiệp vừa và lớn hoạt động trong nhiều ngành nghề bao gồm y tế, truyền thông và giải trí...'
        ],
        'value'=>[
            'title'=>'Giá trị cốt lõi',
            'description'=>'',
            'one'=>[
                'title'=>'Không ngừng sáng tạo',
                'description'=>'Chúng tôi không ngừng tìm tòi, học hỏi và sáng tạo để tạo ra kết quả tốt nhất.'
            ],
            'two'=>[
                'title'=>'Giàu nhiệt huyết',
                'description'=>'Đội ngũ nhân viên luôn nhiệt huyết, cháy hết mình trong công việc và các hoạt động của công ty.'
            ],
            'three'=>[
                'title'=>'Chu đáo',
                'description'=>'Chúng tôi luôn chu đáo với mọi sản phẩm, dịch vụ, với đối tác, khách hàng và các thành viên trong công ty.'
            ],
            'four'=>[
                'title'=>'Luôn luôn phát triển',
                'description'=>'Chúng tôi hướng tới sự phát triển của công ty, đối tác, khách hàng và nhân sự.'
            ],
        ]
    ],
    'service'=>[
    	'banner'=>[
    		'title'=>'Dịch vụ',
    		'description'=>'',
    		'one'=>[
    			'title'=>'Chất lượng',
    			'description'=>''
    		],
    		'two'=>[
    			'title'=>'Đa dạng',
    			'description'=>''
    		],
    		'three'=>[
    			'title'=>'Sáng tạo',
    			'description'=>''
    		],
    	],
    	'top'=>[
    		'title'=>'Mang đến trải nghiệm tốt nhất cho khách hàng',
    		'description'=>'Với sự bùng nổ của các loại điện thoại thông minh và thế hệ mạng 3G/4G, khách hàng luôn tìm kiếm những dịch vụ mới, thú vị và sử dụng công nghệ hiện đại. Với hơn 10 năm kinh nghiệm, ICOM không ngừng tìm tòi, nghiên cứu và áp dụng kiến thức, công nghệ mới để cho ra đời những dịch vụ đặc sắc, mang nhiều lợi ích cho khách hàng.'
    	],
    	'main'=>[
    		'title'=>'',
    		'description'=>'',
    		'one'=>[
    			'title'=>'Dịch vụ Giá trị gia tăng',
    			'description'=>'Là một trong những đơn vị đi đầu trong lĩnh vực giá trị gia tăng, trong suốt hơn 10 năm qua, chúng tôi là đối tác chiến lược của các công ty viễn thông di động như Viettel, MobiFone, Vinaphone... cung cấp hàng trăm dịch vụ giá trị gia tăng cho hàng triệu người dùng tại Việt Nam'
    		],
    		'two'=>[
    			'title'=>'PayTV',
    			'description'=>'Với TVPlay - dịch vụ tương tác truyền hình trên đa nền tảng khách hàng hoàn toàn có thể biến chiếc smartphone bỏ túi thông dụng thành một chiếc smart TV di động, phục vụ nhu cầu xem truyền hình miễn cước data ở bất cứ nơi đâu và bất cứ khi nào với chi phí cực kì tiết kiệm.'
    		],
    		'three'=>[
    			'title'=>'Giải pháp Marketing',
    			'description'=>'ICOM là công ty hàng đầu về quảng cáo trực tuyến tại Việt Nam. ICOM vinh dự là đối tác chiến lược của Google để phát triển dịch vụ quảng cáo trực tuyến và dịch vụ dữ liệu lớn ở Việt Nam, một trong con số hiếm hoi đối tác chính thức của Google ở khu vực Đông Nam Á.'
    		],
    		'five'=>[
    			'title'=>'Nội dung số',
    			'description'=>'Emdep và Myidol là hai trang báo điện tử hàng đầu về phụ nữ và giải trí tại Việt Nam, được ICOM sở hữu và vận hành, phục vụ hàng triệu độc giả ở Việt Nam'
    		],
    		'four'=>[
    			'title'=>'Giải pháp thanh toán',
    			'description'=>'ICOM là nhà phát triển các giải pháp thanh toán giúp cải thiện trải nghiệm người dùng. Thông qua các giải pháp này, thuê bao di động có thể ứng trước tiền, cuộc gọi/tin nhắn SMS/3G hoặc 4G để sử dụng.'
    		],
    	],
    	'contact'=>[
    		'title'=>'Liên hệ với chúng tôi',
    		'description'=>'Hãy liên hệ với chúng tôi để tìm hiểu & hợp tác phát triển các dịch vụ'
    	]
    ],
    'contact'=>[
    	'banner'=>[
    		'title'=>'Xin chào!',
    		'description'=>'Hãy để lại lời nhắn cho chúng tôi'
    	],
    	'form'=>[
    		'title'=>'Contact us',
    		'label'=>[
    			'name'=>'Họ và tên',
    			'phone'=>'Số điện thoại',
    			'email'=>'Địa chỉ hòm thư',
    			'company'=>'Tên cơ quan',
    			'message'=>'Lời nhắn'
    		]
    	]
    ],
    'news'=>[
        'banner'=>[
            'title'=>'"CHUYỂN ĐỘNG" CÙNG THỜI GIAN',
            'description'=>''
        ]
    ],
    'button'=>[
    	'more'=>'Xem thêm',
    	'contact'=>'Liên hệ',
    	'send'=>'Gửi lời nhắn'
    ]
];
