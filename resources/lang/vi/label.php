<?php
/**
 * Created by PhpStorm.
 * User: HiepHM
 * Date: 11/17/2017
 * Time: 11:12 AM
 */
return [
    'add' => 'Thêm mới',
    'update' => 'Cập nhật',
    'reset' => 'Làm lại',
    'note' => [
        'required' => 'Những trường bắt buộc không để trống'
    ],
    'created_at' => 'Ngày tạo',
    'updated_at' => 'Ngày làm mới',
    'module' => [
        'code' => 'Mã Module',
        'name' => 'Tên Module',
        'nameEn' => 'Tên tiếng Anh',
        'alias' => 'Tên khác',
        'description' => 'Mô tả',
        'active' => 'Kích hoạt',
        'order' => 'Thứ tự'
    ],
    'category' => [
        'name' => 'Tên danh mục',
        'nameEn' => 'Tên tiếng Anh',
        'alias' => 'Tên khác',
        'description' => 'Mô tả',
        'active' => 'Kích hoạt',
        'order' => 'Thứ tự',
        'catParent' => 'Danh mục cha'
    ],
    'post'=>[
        'title' => 'Tiêu đề',
        'slug'=> 'Khóa',
        'description'=>'Mô tả',
        'content' =>'Nội dung',
        'keywords' => 'Từ khóa SEO',
        'catId'=> 'Danh mục',
        'catTag'=>'Danh mục liên quan',
        'tag' => 'Tags',
        'avatar' => 'Ảnh đại diện',
        'stage' => 'Trạng thái',
        'active' => 'Kích hoạt',
        'activeTime' => 'Thời gian kích hoạt',
        'option' => 'Thuộc tính',
        'source' => 'Nguồn',
        'sourceUrl' => 'Link nguồn',
        'partner' => 'Đối tác',
        'publish' => 'Ngày đăng'
    ],
    'menu'=>[
        'name'=>'Tên menu',
        'nameEn'=>'Tên tiếng Anh',
        'alias'=>'Viết tắt',
        'link'=>'Đường dẫn',
        'active'=>'Kích hoạt',
        'icon'=>'Biểu tượng',
        'order'=>'Thứ tự'
    ],
    'infosite'=>[
        'title'=>'Tiêu đề',
        'titleEn'=>'Tiêu đề Anh',
        'description'=>'Mô tả',
        'descriptionEn'=>'Mô tả Anh',
        'active'=>'Kích hoạt',
        'order'=>'Thứ tự',
        'page'=>'Tại trang',
        'position'=>'Vị trí',
        'sub'=>'Cấp 2'
    ],
    'service'=>[
        'title'=>'Tiêu đề',
        'titleEn'=>'Tiêu đề TA',
        'description'=>'Mô tả',
        'descriptionEn'=>'Mô tả TA',
        'quote'=>'Trích dẫn',
        'quoteEn'=>'Trích dẫn TA',
        'content'=>'Nội dung',
        'contentEn'=>'Nội dung TA',
        'avatar'=>'Ảnh đại diện',
        'image'=>'Ảnh nền',
        'active'=>'Kích hoạt',
        'hot'=>'Nổi bật',
        'home'=>'Trang chủ',
        'order'=>'Thứ tự'
    ]
];
