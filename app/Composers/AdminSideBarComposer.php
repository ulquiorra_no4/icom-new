<?php
/**
 * Created by PhpStorm.
 * User: HiepHM
 * Date: 11/22/2017
 * Time: 10:16 AM
 */

namespace App\Composers;

use App\Models\Modules\ModuleRepository;
use Illuminate\View\View;

class AdminSideBarComposer
{
    protected $_sidebar_composer;

    public function __construct(ModuleRepository $moduleRepository)
    {
        // $this->_sidebar_composer = $moduleRepository->fetchAll();
    }

    public function compose(View $view)
    {
        // $view->with('sidebar_composer', $this->_sidebar_composer);
    }
}
