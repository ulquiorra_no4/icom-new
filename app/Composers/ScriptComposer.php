<?php
/**
 * Created by PhpStorm.
 * User: HiepHM
 * Date: 11/22/2017
 * Time: 4:04 PM
 */

namespace App\Composers;

use App\Helpers\ScriptHelper;
use Illuminate\View\View;

class ScriptComposer
{

    protected $_script;

    public function __construct(ScriptHelper $scriptHelper)
    {
        $this->_script = $scriptHelper->script();
    }

    public function compose(View $view)
    {
        $view->with('script', $this->_script);
    }
}
