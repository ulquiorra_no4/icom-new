<?php

namespace App\Policies;

use App\Models\Users\Admin as User;
use Illuminate\Auth\Access\HandlesAuthorization;
use
class AdminPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the admin.
     *
     * @param  \App\Models\Users\Admin  $user
     * @return mixed
     */
    public function view(User $user)
    {
        return $user->role==='administrator'||$user->role==='admin';
    }

    /**
     * Determine whether the user can create admins.
     *
     * @param  \App\Models\Users\Admin  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->role==='administrator';
    }

    /**
     * Determine whether the user can update the admin.
     *
     * @param  \App\Models\Users\Admin
     * @param  \App\Admin  $admin
     * @return mixed
     */
    public function update(User $user)
    {
        return $user->role==='administrator'||$user->id===Auth::guard('admin')->user()->id;
    }

    /**
     * Determine whether the user can delete the admin.
     *
     * @param  \App\Models\Users\Admin
     * @return mixed
     */
    public function delete(User $user)
    {
        return $user->role==='administrator';
    }
}
