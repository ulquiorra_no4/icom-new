<?php

namespace App\Policies;

use App\Models\Users\Admin;
use App\Models\Posts\Posts;
use Illuminate\Auth\Access\HandlesAuthorization;

class PostPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    public function view(){}
    public function create(Admin $admin)
    {
        return $admin->role==='administrator';
    }
    public function update(Admin $admin,Posts $posts)
    {
        return $admin->role==='administrator'||$admin->role==='admin'||$admin->id===$posts->author_id;
    }
    public function delete(Admin $admin)
    {
        return $admin->role==='administrator';
    }
}
