<?php
/**
 * Created by PhpStorm.
 * User: HiepHM
 * Date: 11/22/2017
 * Time: 8:59 AM
 */

namespace App\Libraries;

class TableHelper
{
    protected $_table_title;
    protected $_sort;
    protected $_filter;
    protected $_data;

    public function title()
    {
        return $this->_table_title;
    }

    public function sort()
    {
        return $this->_sort;
    }

    public function filter()
    {
        return $this->_filter;
    }

    public function data()
    {
        return $this->_data;
    }
}