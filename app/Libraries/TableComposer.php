<?php
/**
 * Created by PhpStorm.
 * User: HiepHM
 * Date: 11/22/2017
 * Time: 9:44 AM
 */

namespace App\Libraries;

use Illuminate\Support\Facades\View;

class TableComposer
{

    public function __construct()
    {
    }

    public function compose(View $view)
    {
        $view->with('menu', [['title' => 'a', 'link' => '#'], ['title' => 'b', 'link' => '##']]);
    }
}