<?php

namespace App\Models\Modules;

use Illuminate\Database\Eloquent\Model;

class Modules extends Model
{
    //
    protected $table = 'modules';

    protected $primaryKey = 'mod_id';

    protected $fillable = ['mod_code', 'mod_name', 'mod_name_en', 'mod_alias', 'mod_description', 'active', 'order'];
}
