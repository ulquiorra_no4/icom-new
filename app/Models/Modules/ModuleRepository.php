<?php

namespace App\Models\Modules;

use Mockery\Exception;
use App\Models\BaseInterface;

class ModuleRepository implements ModuleInterface, BaseInterface
{
    protected $_model;
    protected $primaryKey = 'mod_id';

    public function __construct(Modules $model)
    {
        $this->_model = $model;
    }

    /**
     * Get all record
     * @return array|\Illuminate\Database\Eloquent\Collection|static[]
     */
    public function fetchAll()
    {
        $data = [];
        // TODO: Implement fetchAll() method.
        try {
            $data = $this->_model->all();
        } catch (Exception $ex) {
        }
        return $data;
    }

    /**
     * Create new record or update a record
     * @param $data
     * @return bool
     */
    public function save($data)
    {
        // TODO: Implement save() method.
        if (!$data) return false;
        try {
            if (isset($data[$this->primaryKey])) {

                $this->_model->findOrFail($data[$this->primaryKey])->update($data);
            } else {

                $this->_model->create($data);
            }
            return true;

        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * Delete a record by Id
     * @param $id
     * @return bool
     */
    public function destroy($id)
    {
        // TODO: Implement destroy() method.
        try {
            $this->_model->findOrFail($id)->delete();

            return true;
        } catch (Exception $ex) {
        }
        return false;
    }

    /**
     * Delete multi record
     * @param $a_id
     * @return bool
     */
    public function multiDestroy($a_id)
    {
        // TODO: Implement multiDestroy() method.
        try {
            $this->_model->whereIn($this->primaryKey, $a_id)->delete();

            return true;
        } catch (Exception $ex) {
        }
        return false;
    }

    public function active($id)
    {
        // TODO: Implement active() method.
    }

    /**
     * Get a record by Id
     * @param $id
     * @return array|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    public function findById($id)
    {
        // TODO: Implement findById() method.\
        $data = [];
        try {
            $data = $this->_model->findOrFail($id);
        } catch (Exception $ex) {
        }
        return $data;
    }
}