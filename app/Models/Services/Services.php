<?php

namespace App\Models\Services;

use Illuminate\Database\Eloquent\Model;

class Services extends Model
{
    protected $table = 'services';
    protected $fillable = ['ser_title','ser_title_en','ser_description','ser_description_en','ser_quote','ser_quote_en','ser_content','ser_content_en','avatar','slug','hot','order','set_home','active','image_thumb'];
    protected $primaryKey = 'ser_id';

    public function scopeActive($query){
        return $query->where('active',1);
    }
    public function scopeSort($query){
        return $query->orderBy('order', 'ASC');
    }
}
