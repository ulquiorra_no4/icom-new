<?php

namespace App\Models\Users;

use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Admin extends Authenticatable
{
    //
    use Notifiable;

    protected $table = 'admin_users';
    protected $primaryKey = 'id';
    protected $fillable = ['name','username','email','phone','address','role','group','password','avatar','active'];

    protected $hidden = ['password','remember_token'];
    protected $remember_token = 'remember_token';

    public function posts(){
        return $this->hasMany('App\Models\Posts\Posts','author_id',$this->primaryKey);
    }

    public function scopeActive($query){
        return $query->where('active', 1);
    }
}
