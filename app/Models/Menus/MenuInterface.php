<?php
namespace App\Models\Menus;
interface MenuInterface{
    public function getMenu();
    public function getPluckMenu($prepend = 'Choose Page', $pull = false);
}
