<?php

namespace App\Models\Categories;
interface CategoryInterface
{
    public function getPluckCategory($active = 1, $prepend = 'Choose category', $pull = false);
}