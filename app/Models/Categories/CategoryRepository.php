<?php

namespace App\Models\Categories;

use App\Models\BaseInterface;
use Mockery\Exception;

class CategoryRepository implements CategoryInterface, BaseInterface
{
    protected $_model;
    protected $primaryKey = 'cat_id';

    public function __construct(Categories $model)
    {
        $this->_model = $model;
    }

    public function fetchAll()
    {
        // TODO: Implement fetchAll() method.
        $data = [];
        try {
            $data = $this->_model->all();
        } catch (Exception $ex) {
        }
        return $data;
    }

    public function findById($id)
    {
        // TODO: Implement findById() method.
        $data = [];
        try {
            $data = $this->_model->findOrFail($id);
        } catch (Exception $ex) {
        }
        return $data;
    }

    public function destroy($id)
    {
        // TODO: Implement destroy() method.
        try {
            $this->_model->findOrFail($id)->delete();

            return true;
        } catch (Exception $ex) {
        }
        return false;

    }

    public function multiDestroy($a_id)
    {
        // TODO: Implement multiDestroy() method.
        try {
            $this->_model->whereIn($this->primaryKey, $a_id)->delete();

            return true;
        } catch (Exception $ex) {
        }
        return false;
    }

    public function active($id)
    {
        // TODO: Implement active() method.
    }

    public function save($data)
    {
        // TODO: Implement save() method.
        if (!$data) return false;
        try {
            if (isset($data[$this->primaryKey])) {

                $this->_model->findOrFail($data[$this->primaryKey])->update($data);
            } else {

                $this->_model->create($data);
            }
            return true;

        } catch (Exception $ex) {
            return false;
        }
    }

    public function getPluckCategory($active = 1, $prepend = 'Choose category', $pull = false)
    {
        // TODO: Implement getPluckCategory() method.
        $data = [];
        try {
            $data = $this->_model->where(['active' => $active])
                ->pluck('cat_name', 'cat_id');
            if ($prepend !== '') {
                $data->prepend($prepend);
            }
            if ($pull){
                $data->pull($pull);
            }
        } catch (Exception $ex) {
        }
        return $data;
    }
}