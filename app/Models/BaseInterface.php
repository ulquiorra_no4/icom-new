<?php
/**
 * Created by PhpStorm.
 * User: HiepHM
 * Date: 11/20/2017
 * Time: 8:28 AM
 */

namespace App\Models;

interface BaseInterface
{
    public function fetchAll();

    public function save($data);

    public function destroy($id);

    public function findById($id);

    public function active($id);

    public function multiDestroy($a_id);

}