<?php
namespace App\Models\Posts;
use App\Models\BaseInterface;
use Mockery\Exception;
class PostRepository implements PostInterface, BaseInterface
{
    protected $_model;
    protected $primaryKey = 'id';

    public function __construct(Posts $model)
    {
        $this->_model = $model;
    }

    public function save($data)
    {
        if (!$data) return false;
        try {
            if (isset($data[$this->primaryKey])) {

                $this->_model->findOrFail($data[$this->primaryKey])->update($data);
            } else {

                $this->_model->create($data);
            }
            return true;

        } catch (Exception $ex) {
            return false;
        }
    }
    public function active($id)
    {
        // TODO: Implement active() method.
    }
    public function multiDestroy($a_id)
    {
        try {
            $this->_model->whereIn($this->primaryKey, $a_id)->delete();

            return true;
        } catch (Exception $ex) {
        }
        return false;
    }
    public function destroy($id)
    {
        try {
            $this->_model->findOrFail($id)->delete();

            return true;
        } catch (Exception $ex) {
        }
        return false;
    }
    public function findById($id)
    {
        $data = [];
        try {
            $data = $this->_model->findOrFail($id);
        } catch (Exception $ex) {
        }
        return $data;
    }
    public function fetchAll()
    {
        $data = [];
        try {
            $data = $this->_model->with('category')->get();
        } catch (Exception $ex) {
        }
        return $data;
    }
    public function getList($where=[],$limit=10){
        $data = [];
        try {
            $data = $this->_model->where($where)->active()->with('category')->latest()->paginate($limit);
        } catch (Exception $e) {

        }
        return $data;
    }
    public function findBySlug($slug=''){
        $slug = trim($slug);
        if(!$slug) return null;
        $data = [];
        try {
            $data = $this->_model->where(['slug'=>$slug])->active()->first();
        } catch (Exception $e) {}
        return $data;
    }
    public function searchPost($keyword='',$where,$limit=10){
        $keyword = trim($keyword);
        if(!$keyword) return null;
        $data = [];
        try {
            $data = $this->_model->search($keyword)->active()->where($where)->paginate($limit);;
        } catch (Exception $e) {}
        return $data;
    }
}
