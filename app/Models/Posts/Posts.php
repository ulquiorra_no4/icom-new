<?php

namespace App\Models\Posts;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    //
    protected $table = 'posts';

    protected $primaryKey = 'id';
    protected $fillable = [
        'title',
        'title_en',
        'slug',
        'description',
        'description_en',
        'content',
        'content_en',
        'keywords',
        'category_id',
        'group_id',
        'cate_tags',
        'tags',
        'avatar',
        'stage',
        'active',
        'active_time',
        'option',
        'author_id',
        'source',
        'source_url',
        'view',
        'partner_id',
        'salary',
        'place',
        'position',
        'position_en',
        'end_time',
        'published_at'
    ];

    public function author(){
        return $this->belongsTo('App\Models\Users\Admin','author_id','id');
    }

    public function category(){
        return $this->belongsTo('App\Models\Categories\Categories','category_id','cat_id');
    }

//    public function partner(){
//        return $this->belongsTo('App\Models\Partners\Partners','partner_id','par_id');
//    }
    /**
     * Scope a query to only include active post.
     * @param $query
     * @return mixed
     */
    public function scopeActive($query){
        return $query->where('active', 1);
    }
    public function scopeSearch($query,$keyword){

        if ($keyword!='') {
            $query->where(function ($query) use ($keyword) {
                $query->where('title', 'LIKE','%'.$keyword.'%')
                    ->orWhere('title_en', 'LIKE', '%'.$keyword.'%')
                    ->orWhere('description', 'LIKE', '%'.$keyword.'%')
                    ->orWhere('description_en', 'LIKE', '%'.$keyword.'%')
                    ->orWhere('content', 'LIKE', '%'.$keyword.'%');
            });
        }
        return $query;
    }
}
