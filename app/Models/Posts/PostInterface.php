<?php
namespace App\Models\Posts;
interface PostInterface{
    public function getList($where = [],$limit=10);
    public function findBySlug($slug='');
    public function searchPost($keyword='',$where,$limit=10);
}
