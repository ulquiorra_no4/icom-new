<?php

namespace App\Models\Roles;

use Illuminate\Database\Eloquent\Model;

class RoleAccess extends Model
{
    //
    protected $table = 'role_access';

    protected $primaryKey = 'rol_id';

    protected $fillable = ['rol_mod_id','rol_user_id','view','add','edit','block','delete','publish','ratify','import','export'];
}
