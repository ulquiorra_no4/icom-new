<?php

namespace App\Models\InfoSites;

use Illuminate\Database\Eloquent\Model;

class InfoSites extends Model
{
    //
    protected $table = 'info_sites';

    protected $fillable = ['inf_title','inf_title_en','inf_description','inf_description_en','inf_page_id','inf_isSub','active','inf_position','order'];

    protected $primaryKey = 'inf_id';

}
