<?php
namespace App\Models\InfoSites;
use App\Models\BaseInterface;
class InfoSiteRepository implements InfoSiteInterface, BaseInterface
{
    protected $_model;
    protected $primaryKey = 'inf_id';
    public function __construct(InfoSites $model)
    {
        $this->_model = $model;
    }

    public function fetchAll(){
        $data = [];
        try {
            $data = $this->_model->all();
        } catch (Exception $ex) {}
        return $data;
    }

    public function save($data){
        if (!$data) return false;
        try {
            if (isset($data[$this->primaryKey])) {

                $this->_model->findOrFail($data[$this->primaryKey])->update($data);
            } else {

                $this->_model->create($data);
            }
            return true;

        } catch (Exception $ex) {}
        return false;
    }

    public function destroy($id){
        try {
            $this->_model->findOrFail($id)->delete();
            return true;
        } catch (Exception $ex) {}
        return false;
    }

    public function findById($id){
        $data = [];
        try {
            $data = $this->_model->findOrFail($id);
        } catch (Exception $ex) {}
        return $data;
    }

    public function active($id){

    }

    public function multiDestroy($a_id){
        try {
            $this->_model->whereIn($this->primaryKey, $a_id)->delete();

            return true;
        } catch (Exception $ex) {}
        return false;
    }
    
}
