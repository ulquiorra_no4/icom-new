<?php
/**
 * Created by PhpStorm.
 * User: HiepHM
 * Date: 11/16/2017
 * Time: 2:35 PM
 */
function generatePathFile($path_file, $file_name)
{
    $date_file = intval(preg_replace("/[^0-9]/i", "", $file_name));
    $new_path = $path_file .'/'. date("Y/m/d/", $date_file);
    // create new folder
    if (!is_dir($new_path)) {
        mkdir($new_path, 0777, true);
        chmod($new_path, 0777);
    }
    return $new_path;
}

function generateFileName()
{
    $name = "";
    for ($i = 0; $i < 3; $i++) {
        $name .= chr(rand(97, 122));
    }
    $today = getdate();
    $name .= $today[0];

    return $name;
}

function genOriginPic($file){
    if(!$file) return '';

    $p = generatePathFile('/pictures/origin',$file);
    return $p.'/'.$file;
}

function genCanvasPic($file,$size='140'){
    if(!$file) return '';

    $p = generatePathFile('/pictures/canvas',$file);
    return $p.'/'.$size.'/'.$file;
}

function url_detail($path='',$slug=''){
    if (!$slug) return '/';
    return '/'.$path.'/'.$slug.'.html';
}
function encryptIt($q) {
    $cryptKey  = 'MYOiTVSY0eHhCJRXKtEhiBk';
    $qEncoded  = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($cryptKey), $q, MCRYPT_MODE_CBC, md5(md5($cryptKey))));
    return $qEncoded;
}
function decryptIt($q) {
    $cryptKey  = 'MYOiTVSY0eHhCJRXKtEhiBk';
    $qDecoded  = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($cryptKey), base64_decode($q), MCRYPT_MODE_CBC, md5(md5($cryptKey))),"\0");
    return $qDecoded;
}