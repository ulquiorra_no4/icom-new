<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InfoSiteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'inf_page_id'=>'required|integer'
        ];
    }

    public function messages()
    {
        return [
            'inf_page_id' => [
                'required' => trans('validation.required'),
                'integer' => trans('validation.integer'),
            ],
        ];
    }
}
