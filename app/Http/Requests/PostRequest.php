<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'title'=>'required|max:255|unique:posts,id',
            'content'=>'required',
            'avatar'=>'image|mimes:jpg,jpeg,png,gif,svg|max:3072',
            'source'=>'max:100'
        ];
    }

    /*
     * Get messages of validation
     * */
    public function messages()
    {
        return [
            'title' => [
                'required' => trans('validation.required'),
                'max' => trans('validation.max.string'),
                'unique' => trans('validation.unique')
            ],
            'content.required' => trans('validation.required'),
            'avatar' => [
                'image' => trans('validation.image'),
                'max' => trans('validation.max.file'),
                'mimes' => trans('validation.mimes')
            ],
            'source.max' => trans('validation.max.string')
        ];
    }
}
