<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ModuleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
            'mod_code' => 'required|max:6|unique:modules,mod_id',
            'mod_name' => 'required|max:50|unique:modules,mod_id',
            'mod_name_en' => 'max:50',
            'order' => 'integer'
        ];
    }

    /*
     * Get messages of validation
     * */
    public function messages()
    {
        return [
            'mod_code' => [
                'required' => trans('validation.required'),
                'max' => trans('validation.max.string'),
                'unique' => trans('validation.unique')
            ],
            'mod_name' => [
                'required' => trans('validation.required'),
                'max' => trans('validation.max.string'),
                'unique' => trans('validation.unique')
            ],
            'mod_name_en.max' => trans('validation.max.string'),
            'order.integer' => trans('validation.integer')
        ];
    }
}
