<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AppController extends Controller
{
    //
    protected $_menu;
    public function __construct(){
        $this->_menu = [
            'home'=>['title'=>trans('app.menu.home'),'link'=>'/'],
            'us'=>['title'=>trans('app.menu.us'),'link'=>'/about'],
            'service'=>['title'=>trans('app.menu.service'),'link'=>'/services'],
            'news'=>['title'=>trans('app.menu.news'),'link'=>'/news'],
            'requi'=>['title'=>trans('app.menu.requi'),'link'=>'/requirement'],
            'contact'=>['title'=>trans('app.menu.contact'),'link'=>'/contact']
        ];
        
    }
}
