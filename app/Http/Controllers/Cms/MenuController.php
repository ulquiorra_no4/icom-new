<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Menus\MenuRepository;
use App\Http\Requests\MenuRequest;
class MenuController extends Controller
{
    protected $_o_menu;
    protected $head_title;
    protected $_table_head;
    public function __construct(MenuRepository $menuRepository){
        $this->_o_menu = $menuRepository;
        $this->_table_head = $this->table_head();
        $this->head_title = trans('table.menu.name');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menus = $this->_o_menu->fetchAll();
        // dd($menus);
        return view('cms.menus.index',['menus'=>$menus,'head_title' => $this->head_title, 'table_head' => $this->_table_head]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('cms.menus.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MenuRequest $request)
    {
        $data = $this->attribute($request->all());
//        dd($data);
        $this->_o_menu->save($data);
        \Session::flash('alert_success', trans('alert.success.add'));
        return redirect()->intended('/admin/menuses/add');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $menu = $this->_o_menu->findById($id);
        return view('cms.menus.edit',['menu'=>$menu]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MenuRequest $request, $id)
    {
        $data = $this->attribute($request->all());
        $data['menu_id'] = (int)$id;
//        dd($data);
        $this->_o_menu->save($data);
        \Session::flash('alert_success', trans('alert.success.update'));
        return redirect()->route('menu');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,Request $request)
    {
        $id = (int)$id;
        $error = 1;
        $mess = '';
        if ($request->ajax()) {
            if ($this->_o_menu->destroy($id)) {
                $error = 0;
                $mess = trans('alert.success.delete');
            } else {
                $error = 1;
                $mess = trans('alert.error.delete');
            }
        }
        $return = [
            'error' => $error,
            'mess' => $mess
        ];
        return response()->json($return);
    }
    /**
     * Delete multi item
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function multi_destroy(Request $request)
    {
        $a_data = $request->only('id');
        $a_id = $a_data['id'];
        $error = 1;
        $mess = '';
        if ($request->ajax()) {
            if ($this->_o_menu->multiDestroy($a_id)) {
                $error = 0;
                $mess = trans('alert.success.delete');
            } else {
                $error = 1;
                $mess = trans('alert.error.delete');
            }
        }
        $return = [
            'error' => $error,
            'mess' => $mess
        ];
        return response()->json($return);
    }
    /**
     * Create form attribute of data
     * @param array $data
     * @return array
     */
    private function attribute(array $data)
    {
        return [
            'menu_name' => $data['menu_name'],
            'menu_name_en' => $data['menu_name_en'],
            'menu_alias' => $data['menu_alias'],
            'menu_link' => $data['menu_link'],
            'menu_icon' => $data['menu_icon'],
            'order' => (int)$data['order'],
            'active' => isset($data['active']) ? (int)$data['active'] : 0
        ];
    }
    /**
     * Create heading for table
     * @return array
     */
    public function table_head()
    {
        return [
            [
                'key' => 'menu_name',
                'name' => trans('label.menu.name'),
                'options' => [
                    'icon' => ''
                ],
            ],
            [
                'key' => 'menu_name_en',
                'name' => trans('label.menu.nameEn'),
                'options' => [
                    'icon' => ''
                ],
            ],
            [
                'key' => 'menu_link',
                'name' => trans('label.menu.link'),
                'options' => [
                    'icon' => ''
                ],
            ],
            [
                'key' => 'active',
                'name' => trans('label.menu.active'),
                'options' => [
                    'icon' => ''
                ],
            ],
            [
                'key' => 'order',
                'name' => trans('label.menu.order'),
                'options' => [
                    'icon' => ''
                ],
            ],
            [
                'key' => 'created_at',
                'name' => trans('label.created_at'),
                'options' => [
                    'icon' => ''
                ],
            ]

        ];
    }
}
