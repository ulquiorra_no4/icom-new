<?php

namespace App\Http\Controllers\Cms;

use App\Models\Categories\CategoryRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest as CatRequest;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $_o_category;
    protected $head_title;
    protected $_table_head;

    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->_o_category = $categoryRepository;
        $this->_table_head = $this->table_head();
        $this->head_title = trans('table.category.name');
    }

    public function index()
    {
        //
        $categories = $this->_o_category->fetchAll();
        // dd($categories);
        return view('cms.categories.index', ['categories' => $categories, 'head_title' => $this->head_title, 'table_head' => $this->_table_head]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $a_cat_parent = $this->_o_category->getPluckCategory();
        return view('cms.categories.add', ['a_cat_parent' => $a_cat_parent]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CatRequest $request)
    {
        //
        $data = $this->attribute($request->all());
//        dd($data);
        $this->_o_category->save($data);
        \Session::flash('alert_success', trans('alert.success.add'));
        return redirect()->intended('/admin/categories/add');

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $a_cat_parent = $this->_o_category->getPluckCategory(1, 'Choose category', $id);
        $category = $this->_o_category->findById($id);

        return view('cms.categories.edit', ['category' => $category, 'a_cat_parent' => $a_cat_parent]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(CatRequest $request, $id)
    {
        //
        $data = $this->attribute($request->all());
        $data['cat_id'] = (int)$id;
//        dd($data);
        $this->_o_category->save($data);
        \Session::flash('alert_success', trans('alert.success.update'));
        return redirect()->route('cat');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        //
        $id = (int)$id;
        $error = 1;
        $mess = '';
        if ($request->ajax()) {
            if ($this->_o_category->destroy($id)) {
                $error = 0;
                $mess = trans('alert.success.delete');
            } else {
                $error = 1;
                $mess = trans('alert.error.delete');
            }
        }
        $return = [
            'error' => $error,
            'mess' => $mess
        ];
        return response()->json($return);
    }

    /**
     * Delete multi item
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function multi_destroy(Request $request)
    {
        $a_data = $request->only('id');
        $a_id = $a_data['id'];
        $error = 1;
        $mess = '';
        if ($request->ajax()) {
            if ($this->_o_category->multiDestroy($a_id)) {
                $error = 0;
                $mess = trans('alert.success.delete');
            } else {
                $error = 1;
                $mess = trans('alert.error.delete');
            }
        }
        $return = [
            'error' => $error,
            'mess' => $mess
        ];
        return response()->json($return);
    }
    /**
     * Create form attribute of data
     * @param array $data
     * @return array
     */
    private function attribute(array $data)
    {
        return [
            'cat_name' => $data['cat_name'],
            'cat_name_en' => $data['cat_name_en'],
            'cat_alias' => $data['cat_alias'],
            'cat_description' => $data['cat_description'],
            'cat_parent_id' => (int)$data['cat_parent_id'],
            'order' => (int)$data['order'],
            'active' => isset($data['active']) ? (int)$data['active'] : 0
        ];
    }

    /**
     * Create heading for table
     * @return array
     */
    public function table_head()
    {
        return [
            [
                'key' => 'cat_name',
                'name' => trans('label.category.name'),
                'options' => [
                    'icon' => ''
                ],
            ],
            [
                'key' => 'cat_name_en',
                'name' => trans('label.category.nameEn'),
                'options' => [
                    'icon' => ''
                ],
            ],
            [
                'key' => 'cat_alias',
                'name' => trans('label.category.alias'),
                'options' => [
                    'icon' => ''
                ],
            ],
            [
                'key' => 'active',
                'name' => trans('label.category.active'),
                'options' => [
                    'icon' => ''
                ],
            ],
            [
                'key' => 'order',
                'name' => trans('label.category.order'),
                'options' => [
                    'icon' => ''
                ],
            ],
            [
                'key' => 'created_at',
                'name' => trans('label.created_at'),
                'options' => [
                    'icon' => ''
                ],
            ],
            [
                'key' => 'updated_at',
                'name' => trans('label.updated_at'),
                'options' => [
                    'icon' => ''
                ],
            ],

        ];
    }
}
