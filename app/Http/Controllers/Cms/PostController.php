<?php

namespace App\Http\Controllers\Cms;

use App\Models\Categories\CategoryRepository;
use App\Models\Posts\PostRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\PostRequest;
use Illuminate\Support\Facades\Auth;
use App\Models\Posts\Posts;
use Image;
use File;
class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    protected $_o_post;
    protected $_o_category;
    protected $head_title;
    protected $_table_head;

    public function __construct(PostRepository $postRepository, CategoryRepository $categoryRepository)
    {
        $this->_o_post = $postRepository;
        $this->_o_category = $categoryRepository;
        $this->head_title = trans('table.post.name');
        $this->_table_head = $this->table_head();
    }

    public function index()
    {
        $posts = $this->_o_post->fetchAll();
        // dd($posts[0]->category->cat_name);
        return view('cms.posts.index',['posts'=>$posts,'head_title' => $this->head_title, 'table_head' => $this->_table_head]);
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Auth::user();
        // dd($user);
        if($user->can('create',Posts::class)){
            echo 'can';die;
            $a_categories = $this->_o_category->getPluckCategory();
            return view('cms.posts.add', ['a_categories' => $a_categories]);
        }else{
            echo 'not can';die;
        }
        
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        $data = $this->attribute($request->all());
        if ($request->hasFile('pos_avatar')) {
            $image = $request->file('pos_avatar');

            $data['avatar'] = generateFileName() . '.' . $image->getClientOriginalExtension();
            $originPath = public_path(generatePathFile(config('picture.originPath'),$data['avatar']));
            $canvasPath = public_path(generatePathFile(config('picture.canvasPath'),$data['avatar']));
            // Create folder if not exit
            File::makeDirectory($canvasPath.'140/', 0775, true, true);
            File::makeDirectory($canvasPath.'200/', 0775, true, true);
            File::makeDirectory($canvasPath.'300/', 0775, true, true);
            // Create thumbnail
            $img = Image::make($image->getRealPath());
            $img->resize(200, 200, function ($constraint) {
                $constraint->aspectRatio();

            })->save($canvasPath.'200/'.$data['avatar']);
            $img->resize(140, 140, function ($constraint) {
                $constraint->aspectRatio();

            })->save($canvasPath.'140/'.$data['avatar']);
            $img->resize(300, 300, function ($constraint) {
                $constraint->aspectRatio();

            })->save($canvasPath.'300/'.$data['avatar']);
            // Create image origin
            $image->move($originPath, $data['avatar']);
        }
        $data['stage'] = 'draft';
        $data['author'] = 1;
        // dd($data);
        if($this->_o_post->save($data)){
            \Session::flash('alert_success', trans('alert.success.add'));
        }
        return redirect()->intended('/admin/posts/add');

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = $this->_o_post->findById($id);

        if (!$post) return redirect()->route('pos');
        $a_categories = $this->_o_category->getPluckCategory();
        return view('cms.posts.edit',['post'=>$post,'a_categories' => $a_categories]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, $id)
    {
        $data = $this->attribute($request->all());
        if ($request->hasFile('pos_avatar')) {
            $image = $request->file('pos_avatar');

            $data['avatar'] = generateFileName() . '.' . $image->getClientOriginalExtension();
            $originPath = public_path(generatePathFile(config('picture.originPath'),$data['avatar']));
            $canvasPath = public_path(generatePathFile(config('picture.canvasPath'),$data['avatar']));
            // Create folder if not exit
            File::makeDirectory($canvasPath.'140/', 0775, true, true);
            File::makeDirectory($canvasPath.'200/', 0775, true, true);
            File::makeDirectory($canvasPath.'300/', 0775, true, true);
            // Create thumbnail
            $img = Image::make($image->getRealPath());
            $img->resize(200, 200, function ($constraint) {
                $constraint->aspectRatio();

            })->save($canvasPath.'200/'.$data['avatar']);
            $img->resize(140, 140, function ($constraint) {
                $constraint->aspectRatio();

            })->save($canvasPath.'140/'.$data['avatar']);
            $img->resize(300, 300, function ($constraint) {
                $constraint->aspectRatio();

            })->save($canvasPath.'300/'.$data['avatar']);
            // Create image origin
            $image->move($originPath, $data['avatar']);
        }
        $data['stage'] = 'draft';
        $data['author'] = 1;
        // dd($data);
        if($this->_o_post->save($data)){
            \Session::flash('alert_success', trans('alert.success.add'));
        }
        return redirect()->route('pos');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,Request $request)
    {
        //
    }

    /**
     * Create form attribute of data
     * @param array $data
     * @return array
     */
    private function attribute(array $data)
    {

        return [
            'title' => $data['title'],
            'slug' => str_slug($data['title'],'-'),
            'description' => $data['description'],
            'content' => $data['content'],
            'keywords' => $data['keywords'],
            'category_id' => (int)$data['category_id'],
            'cate_tags' => $data['cate_tags'],
            'tags' => $data['tags'],
//            'stage' => $data['pos_stage'],
            'active_time' => $data['active_time'],
            'option' => $data['option'],
            'source' => $data['source'],
            'source_url' => $data['source_url'],
            'partner_id' => (int)$data['partner_id'],
            'active' => isset($data['active']) ? (int)$data['active'] : 0
        ];
    }

    /**
     * Create heading for table
     * @return array
     */
    public function table_head()
    {
        return [
            [
                'key' => 'title',
                'name' => trans('label.post.title'),
                'options' => [
                    'icon' => ''
                ],
            ],
            [
                'key' => 'category',
                'name' => trans('label.post.catId'),
                'options' => [
                    'icon' => ''
                ],
            ],
            [
                'key' => 'active',
                'name' => trans('label.post.active'),
                'options' => [
                    'icon' => ''
                ],
            ],
            [
                'key' => 'published_at',
                'name' => trans('label.post.publish'),
                'options' => [
                    'icon' => ''
                ],
            ],
            [
                'key' => 'created_at',
                'name' => trans('label.created_at'),
                'options' => [
                    'icon' => ''
                ],
            ]
        ];
    }
}
