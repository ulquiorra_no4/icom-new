<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ServiceRequest;
use App\Models\Services\ServiceRepository;
use Illuminate\Http\UploadedFile;
class ServiceController extends Controller
{
    protected $_o_service;
    protected $head_title;
    protected $_table_head;
    public function __construct(ServiceRepository $serviceRepository){
        $this->_o_service = $serviceRepository;
        $this->head_title = trans('table.service.name');
        $this->_table_head = $this->table_head();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $services = $this->_o_service->fetchAll();
        return view('cms.services.index',['services'=>$services,'head_title' => $this->head_title, 'table_head' => $this->_table_head]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('cms.services.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ServiceRequest $request)
    {
        $data = $this->attribute($request->all());
        if ($request->hasFile('ser_avatar')) {
            $image = $request->file('ser_avatar');

            $data['avatar'] = generateFileName() . '.' . $image->getClientOriginalExtension();
            $originPath = public_path(generatePathFile(config('picture.originPath'),$data['avatar']));
            // dd($request->ser_avatar);
            // Create image origin
            $image->move($originPath, $data['avatar']);
        }
        if($request->hasFile('ser_thumbnail')){
            $thumb = $request->file('ser_thumbnail');

            $data['image_thumb'] = generateFileName() . '.' . $thumb->getClientOriginalExtension();
            $originPath = public_path(generatePathFile(config('picture.originPath'),$data['image_thumb']));
            $thumb->move($originPath, $data['image_thumb']);
        }
        // dd($data);
        if($this->_o_service->save($data)){
            \Session::flash('alert_success', trans('alert.success.add'));
        }
        return redirect()->intended('/admin/services/add');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service = $this->_o_service->findById($id);
        if (!$service) return redirect()->route('ser');
        return view('cms.services.edit',['service'=>$service]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ServiceRequest $request, $id)
    {
        $data = $this->attribute($request->all());
        if ($request->hasFile('ser_avatar')) {
            $image = $request->file('ser_avatar');

            $data['avatar'] = generateFileName() . '.' . $image->getClientOriginalExtension();
            $originPath = public_path(generatePathFile(config('picture.originPath'),$data['avatar']));
            // Create image origin
            $image->move($originPath, $data['avatar']);
        }
        if($request->hasFile('ser_thumbnail')){
            $thumb = $request->file('ser_thumbnail');

            $data['image_thumb'] = generateFileName() . '.' . $thumb->getClientOriginalExtension();
            $originPath = public_path(generatePathFile(config('picture.originPath'),$data['image_thumb']));
            $thumb->move($originPath, $data['image_thumb']);
        }
        $data['ser_id'] = (int)$id;
        // dd($data);
        if($this->_o_service->save($data)){
            \Session::flash('alert_success', trans('alert.success.add'));
        }
        return redirect()->route('ser');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,Request $request)
    {
        $id = (int)$id;
        $error = 1;
        $mess = '';
        if ($request->ajax()) {
            if ($this->_o_service->destroy($id)) {
                $error = 0;
                $mess = trans('alert.success.delete');
            } else {
                $error = 1;
                $mess = trans('alert.error.delete');
            }
        }
        $return = [
            'error' => $error,
            'mess' => $mess
        ];
        return response()->json($return);
    }
    /**
     * Delete multi item
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function multi_destroy(Request $request)
    {
        $a_data = $request->only('id');
        $a_id = $a_data['id'];
        $error = 1;
        $mess = '';
        if ($request->ajax()) {
            if ($this->_o_service->multiDestroy($a_id)) {
                $error = 0;
                $mess = trans('alert.success.delete');
            } else {
                $error = 1;
                $mess = trans('alert.error.delete');
            }
        }
        $return = [
            'error' => $error,
            'mess' => $mess
        ];
        return response()->json($return);
    }
    /**
     * Create form attribute of data
     * @param array $data
     * @return array
     */
    private function attribute(array $data)
    {
        return [
            'ser_title' => $data['ser_title'],
            'ser_title_en' => $data['ser_title_en'],
            'slug'=>str_slug($data['ser_title']),
            'ser_description' => $data['ser_description'],
            'ser_description_en' => $data['ser_description_en'],
            'ser_quote' => $data['ser_quote'],
            'ser_quote_en' => $data['ser_quote_en'],
            'ser_content' => $data['ser_content'],
            'ser_content_en' => $data['ser_content_en'],
            'order' => (int)$data['order'],
            'active' => isset($data['active']) ? (int)$data['active'] : 0,
            'hot' => isset($data['hot']) ? (int)$data['hot'] : 0,
            'set_home' => isset($data['set_home']) ? (int)$data['set_home'] : 0
        ];
    }

    /**
     * Create heading for table
     * @return array
     */
    public function table_head()
    {
        return [
            [
                'key' => 'ser_title',
                'name' => trans('label.service.title'),
                'options' => [
                    'icon' => ''
                ],
            ],
            [
                'key' => 'ser_description',
                'name' => trans('label.service.description'),
                'options' => [
                    'icon' => ''
                ],
            ],
            [
                'key' => 'active',
                'name' => trans('label.service.active'),
                'options' => [
                    'icon' => ''
                ],
            ],
            [
                'key' => 'set_home',
                'name' => trans('label.service.home'),
                'options'=>[
                    'icon'=>''
                ]
            ],
            [
                'key' => 'order',
                'name' => trans('label.service.order'),
                'options' => [
                    'icon' => ''
                ],
            ],
            [
                'key' => 'created_at',
                'name' => trans('label.created_at'),
                'options' => [
                    'icon' => ''
                ],
            ],
        ];
    }
}
