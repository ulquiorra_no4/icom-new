<?php

namespace App\Http\Controllers\Cms;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\InfoSites\InfoSiteRepository;
use App\Http\Requests\InfoSiteRequest;
use App\Models\Menus\MenuRepository;

class InfoSiteController extends Controller
{
    protected $_o_infosite;
    protected $_o_menu;
    protected $head_title;
    protected $_table_head;

    public function __construct(InfoSiteRepository $infoSiteRepository,MenuRepository $menuRepository){

        $this->_o_infosite = $infoSiteRepository;
        $this->_o_menu = $menuRepository;
        $this->_table_head = $this->table_head();
        $this->head_title = trans('table.infosite.name');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $infos = $this->_o_infosite->fetchAll();
        // dd($infos);
        return view('cms.infosites.index',['infos'=>$infos,'head_title' => $this->head_title, 'table_head' => $this->_table_head]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $positions = ['mid'=>'mid','banner'=>'banner','top'=>'top','bottom'=>'bottom'];
        $menus = $this->_o_menu->getPluckMenu();
        // dd($menus);
        return view('cms.infosites.add',['menus'=>$menus,'positions'=>$positions]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(InfoSiteRequest $request)
    {
        $data = $this->attribute($request->all());
       // dd($data);
        $this->_o_infosite->save($data);
        \Session::flash('alert_success', trans('alert.success.add'));
        return redirect()->intended('/admin/infosites/add');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $positions = ['mid'=>'mid','banner'=>'banner','top'=>'top','bottom'=>'bottom'];
        $menus = $this->_o_menu->getPluckMenu();
        $detail = $this->_o_infosite->findById($id);
        // dd($detail);
        return view('cms.infosites.edit',['detail'=>$detail,'menus'=>$menus,'positions'=>$positions]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(InfoSiteRequest $request, $id)
    {
        $data = $this->attribute($request->all());
        $data['inf_id'] = (int)$id;
//        dd($data);
        $this->_o_infosite->save($data);
        \Session::flash('alert_success', trans('alert.success.update'));
        return redirect()->route('inf');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,Request $request)
    {
        $id = (int)$id;
        $error = 1;
        $mess = '';
        if ($request->ajax()) {
            if ($this->_o_infosite->destroy($id)) {
                $error = 0;
                $mess = trans('alert.success.delete');
            } else {
                $error = 1;
                $mess = trans('alert.error.delete');
            }
        }
        $return = [
            'error' => $error,
            'mess' => $mess
        ];
        return response()->json($return);
    }
    /**
     * Delete multi item
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function multi_destroy(Request $request)
    {
        $a_data = $request->only('id');
        $a_id = $a_data['id'];
        $error = 1;
        $mess = '';
        if ($request->ajax()) {
            if ($this->_o_infosite->multiDestroy($a_id)) {
                $error = 0;
                $mess = trans('alert.success.delete');
            } else {
                $error = 1;
                $mess = trans('alert.error.delete');
            }
        }
        $return = [
            'error' => $error,
            'mess' => $mess
        ];
        return response()->json($return);
    }
    /**
     * Create form attribute of data
     * @param array $data
     * @return array
     */
    private function attribute(array $data)
    {
        return [
            'inf_title' => $data['inf_title'],
            'inf_title_en' => $data['inf_title_en'],
            'inf_description' => $data['inf_description'],
            'inf_description_en' => $data['inf_description_en'],
            'inf_page_id' => (int)$data['inf_page_id'],
            'inf_position' => $data['inf_position'],
            'order' => (int)$data['order'],
            'inf_isSub' => isset($data['inf_isSub']) ? (int)$data['inf_isSub'] : 0,
            'active' => isset($data['active']) ? (int)$data['active'] : 0
        ];
    }
    /**
     * Create heading for table
     * @return array
     */
    public function table_head()
    {
        return [
            [
                'key' => 'inf_title',
                'name' => trans('label.infosite.title'),
                'options' => [
                    'icon' => ''
                ],
            ],
            [
                'key' => 'inf_title_en',
                'name' => trans('label.infosite.titleEn'),
                'options' => [
                    'icon' => ''
                ],
            ],
            [
                'key' => 'inf_page_id',
                'name' => trans('label.infosite.page'),
                'options' => [
                    'icon' => ''
                ],
            ],
            [
                'key' => 'inf_position',
                'name' => trans('label.infosite.position'),
                'options' => [
                    'icon' => ''
                ],
            ],
            [
                'key' => 'active',
                'name' => trans('label.infosite.active'),
                'options' => [
                    'icon' => ''
                ],
            ],
            [
                'key' => 'order',
                'name' => trans('label.infosite.order'),
                'options' => [
                    'icon' => ''
                ],
            ]

        ];
    }
}
