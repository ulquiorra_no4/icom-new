<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;

class AdLoginController extends Controller
{
    //
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'admin';

    protected $guard = 'admin';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('guest')->except('logout');
        // $this->middleware('admin')->except('logout');
    }

    /*
     * Show login form Admin
     * */
    public function showLogin()
    {
        return view('cms.login');
    }

    /*
     * Logout admin
     * */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect('/admin');
    }

    /*
     * Set the guard Admin
     * */
    protected function guard()
    {
        return Auth::guard($this->guard);
    }

    /*
     * Set login with username
     * */
    public function username()
    {
        return 'username';
    }

    /*
     * Set field active when login
     * */
    public function active()
    {
        return 'active';
    }

    public function password()
    {
        return 'password';
    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    protected function validateLogin(Request $request)
    {
        $this->validate($request, [
            'username' => 'required|string',
            'password' => 'required|string',
        ]);
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        // user active
        $active = [];
        if (method_exists($this, 'active')) {
            $active = [$this->active() => 1];
        }
        return array_merge($request->only($this->username(), $this->password()), $active);
//        return $request->only($this->username(), 'password');
    }

}
