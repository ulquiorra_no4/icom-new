<?php

namespace App\Http\Controllers\Sites;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\AppController;

class ServiceController extends AppController
{
    //
    protected $_mact;
    protected $_a_services;
    protected $_locale = 'vi';
    public function __construct(){
        parent::__construct();
        $this->_mact = 2;
        $this->_a_services = [
            str_slug('DỊCH VỤ GIÁ TRỊ GIA TĂNG')=>[
                'en'=>[
                    'title'=>'Dịch vụ giá trị gia tăng',
                    'description'=>'',
                    'description_2'=>'',
                    'content'=>'',
                    'thumbnail'=>'/pictures/service06.png',
                    'banner_thumb'=>'/pictures/banner_value.jpg'
                ],
                'vi'=>[
                    'title'=>'Dịch vụ giá trị gia tăng',
                    'description'=>'Dịch vụ giá trị gia tăng (Value Added Service - VAS) trên nền tảng viễn thông di động là một khái niệm mới nghe thì tưởng là một lĩnh vực kinh doanh rất xa lạ với người dùng. Nhưng thực tế, dịch vụ giá trị gia tăng trên nền tảng viễn thông di động tại Việt Nam đang phát triển mạnh mẽ cùng với sự bùng nổ của các loại điện thoại thông minh và thế hệ mạng 3G/4G. Bên cạnh những dịch vụ viễn thông truyền thống là nghe, gọi và nhắn tin, người dùng đã và đang sử dụng rất nhiều các dịch vụ giá trị gia tăng như: Thông báo cuộc gọi nhỡ, Tin tức, Chặn cuộc goi, Nhạc chờ,...',
                    'description_2'=>'Là một trong những đơn vị đi đầu trong lĩnh vực giá trị gia tăng, trong suốt hơn 10 năm qua, chúng tôi là đối tác chiến lược của các công ty viễn thông di động tại Việt Nam như Viettel, MobiFone, Vinaphone... cung cấp hàng trăm dịch vụ giá trị gia tăng cho hàng triệu người dùng, thu về hơn 50 triệu đô la doanh thu người dùng cuối mỗi năm.

Đội ngũ Kỹ thuật, Nội dung và Phân tích thị trường của chúng tôi không ngừng tìm tòi, nghiên cứu và áp dụng kiến thức, công nghệ mới để cho ra đời những dịch vụ đặc sắc, mang nhiều lợi ích cho khách hàng.',
                    'content'=>[
                        'title'=> 'Một số dịch vụ Giá trị gia tăng nổi bật của chúng tôi:',
                        'description'=>'',
                        'one'=>[
                            'title'=>'Dịch vụ Quản lý cuộc gọi',
                            'description'=>'Dịch vụ quản lý cuộc gọi là một dịch vụ tiện ích giúp các thuê bao di động chủ động kiểm soát được những cuộc gọi đến theo danh sách đã tạo; chuyển các cuộc gọi tới các số thuê bao trong danh sách đã tạo hoặc chủ động nhận cuộc gọi theo ý muốn; chủ động từ chối tất cả các cuộc gọi trong khoảng thời gian nhất định theo ý muốn…với mức cước dịch vụ rất tiết kiệm.'
                        ],
                        'two'=>[
                            'title'=>'Dịch vụ MobiRadio',
                            'description'=>'MobiRadio là dịch vụ nghe Radio trên thiết bị di động, chỉ cần có sóng di động, không cần Internet, không cần điện thoại thông minh, không cần nhớ tần số phát thanh… Khách hàng hoàn toàn có thể chủ động theo dõi trực tiếp các kênh radio trên toàn quốc, nghe lại các chương trình đặc sắc; theo dõi các chương trình theo từng chủ đề, lưu chương trình vào mục yêu thích, giới thiệu chương trình cho bạn bè. Ngoài ra khách hàng còn được thỏa sức trò chuyện với thuê bao nội mạng khác mà không bị giới hạn thời gian gọi với mức cước ưu đãi, gửi quà tặng âm nhạc cho bạn bè cùng lời nhắn gửi yêu thương, nghe các bản tin tổng hợp mới nhất về thời sự chính trị trong ngày, các bản tin về nông nghiệp, nông thôn...'
                        ],
                        'three'=>[
                            'title'=>'Dịch vụ mStudy',
                            'description'=>'Dịch vụ cung cấp các thông tin về giáo dục và học tập kiến thức trong chương trình THCS, THPT, ôn luyện kỳ thì chuyển cấp, thi ĐH-CĐ, tích lũy các kĩ năng mềm, chương trình dạy nghề hướng nghiệp, ngoại ngữ, công nghệ thông tin... dành cho mọi lứa tuổi qua hệ thống bài giảng được truyền tải dưới dạng câu hỏi trắc nghiệm, Video, Clip, Bản đồ tư duy… người học tiếp cận kiến thức qua các kênh SMS, Web, Wap, Ứng dụng, thoại.'
                        ],
                        'four'=>[
                            'title'=>'Dịch vụ mCare',
                            'description'=>'mCare là dịch vụ  cung cấp các thông tin hữu ích về y tế, sức khỏe và dinh dưỡng, đồng thời là nơi chia sẻ thông tin của cộng đồng. Đặc biệt dịch vụ hướng tới các chương trình tương tác nhằm phổ cập kiến thức và giúp khách hàng kiểm tra kiến thức chăm sóc sức khỏe. Ngoài ra một số chương trình kết hợp với Bộ y tế để thực hiện các chiến lược truyền thông quốc gia.'
                        ]
                    ],
                'thumbnail'=>'/pictures/service06.png',
                'banner_thumb'=>'/pictures/banner_value.jpg'
                ]
            ],
            str_slug('PAYTV')=>[
                'en'=>[
                    'title'=>'PAYTV',
                    'description'=>'',
                    'description_2'=>'',
                    'content'=>'',
                    'thumbnail'=>'/pictures/service06.png',
                    'banner_thumb'=>'/pictures/banner_paytv.jpg'
                ],
                'vi'=>[
                    'title'=>'PAYTV',
                    'description'=>'Năm 2017, ICOM vinh dự trở thành 1 trong số ít doanh nghiệp ở Việt Nam được Bộ Thông tin và Truyền thông cấp giấy phép kinh doanh truyền hình trả tiền.

Ngay sau đó, ICOM cho ra mắt TVPlay - dịch vụ tương tác truyền hình trên đa nền tảng (Web, mobile web, mobile app). Ngoài tính năng cho phép người dùng xem trực tiếp các kênh truyền hình trên tivi, các chương trình theo yêu cầu, dịch vụ TVPlay còn cho phép khách hàng tương tác thông qua màn hình điện thoai, tablet ... bằng cách trả lời trực tiếp các câu hỏi, dự đoán tình huống phim,.... và có cơ hội trúng nhiều phần quà có giá trị hấp dẫn.',
                    'description_2'=>'Với TVPlay, khách hàng hoàn toàn có thể biến chiếc smartphone bỏ túi thông dụng thành một chiếc smart TV di động, phục vụ nhu cầu xem truyền hình miễn cước data ở bất cứ nơi đâu và bất cứ khi nào với chi phí cực kì tiết kiệm.

Hiện tại, ICOM đang hợp tác với MobiFone, Viettel và MobiFone để cung cấp dịch vụ này tới hàng triệu khách hàng ở Việt Nam.

TVPlay mang đến thế giới giải trí đa sắc màu với kho nội dung hấp dẫn: kênh truyền hình trực tuyến, phim, video hài, thể thao, TV Show. TVPlay cung cấp cho khách hàng trải nghiệm xem truyền hình, phim và video đỉnh cao với chất lượng tốt nhất và nội dung có bản quyền.',
                    'content'=>'Với thông điệp “Xem cực đã - Kết nối thả ga” TVPlay mang đến cho người dùng những trải nghiệm tuyệt vời và đầy thú vị trên nhiều thiết bị khác nhau và bất cứ nơi đâu.
1. Kho nội dung phong phú không giới hạn đáp ứng sở thích của nhiều đối tượng khách hàng, mở ra thế giới giải trí đầy sống động.
- Nhóm nội dung và thể loại đặc sắc:
+Phim Hoa Ngữ, Hàn Quốc, …thuộc nhiều thể loại hành động, tâm lý, hài hước, cổ trang, ngôn tình..vv
+Phim Việt Nam: Phim truyền hình nổi tiếng và phim điện ảnh
+Hài: với nhiều thể loại tiểu phẩm, show hài hước, clip chế.
+TV Show: Cập nhật chương trình mới nhất như: The Face, Viet Nam Next top Model, Giọng Hát Việt, Thần tượng Bolero...vv
+Thể thao: Các giải thể thao trong nước và thế giới: bóng đá, tennis, bóng chuyền..vv
-Truyền hình với gần 100 kênh truyền hình, trong đó có hơn 20 kênh chất lượng HD:
+VTV
+VTC
+Kênh địa phương
+Kênh thiết yếu

2. Tính năng thông minh, phục vụ tối đa lợi ích người tiêu dùng
Gợi ý chương trình TV hot “Xem gì hôm nay” để bạn không bỏ lỡ chương trình yêu thích.
Hiển thị lịch phát sóng update real time ngay bên dưới frame phát video live, trong lịch phát sóng có đổi màu và hiện nút live ở tên chương trình đang phát sóng.
Xem lại (Catch-up) các chương trình yêu thích.
Tạm dừng kênh live (Timeshift) và xem tiếp ở thời điểm khác.
Ghi lại (Record ) nội dung yêu thích vào thư mục cá nhân.
Tự động chọn chất lượng video tùy thuộc vào tốc độ mạng.
Bình luận khi xem VOD.
Gợi ý thông minh các nội dung phù hợp và yêu thích dựa theo nhu cầu của từng khách hàng.

3. Nội dung bản quyền, chỉ có trên TVPlay
TVPlay trân trọng cung cấp cho người dùng một dịch vụ nội dung hay và thực sự có chất lượng, mua bản quyền từ nhiều đơn vị sản xuất nội dung. Bên cạnh đó, đội ngũ BTV luôn sáng tạo tìm tòi, đầu tư sản xuất với nhiều nội dung mới lạ: phim, clip độc lạ, tin tức cập nhật, review phim hot..

Để biết thêm chi tiết, vui lòng truy cập website dịch vụ: tvplay.vn',
                'thumbnail'=>'/pictures/service06.png',
                'banner_thumb'=>'/pictures/banner_paytv.jpg'
                ]
            ],
            str_slug('GIẢI PHÁP MAKETING')=>[
                'en'=>[
                    'title'=>'Giải pháp Marketing',
                    'description'=>'',
                    'description_2'=>'',
                    'content'=>'',
                    'thumbnail'=>'/pictures/service06.png',
                    'banner_thumb'=>'/pictures/banner_marketing.jpg'
                ],
                'vi'=>[
                    'title'=>'Giải pháp Marketing',
                    'description'=>'ICOM là công ty hàng đầu về quảng cáo trực tuyến tại Việt Nam. ICOM vinh dự là đối tác chiến lược của Google để phát triển dịch vụ quảng cáo trực tuyến và dịch vụ dữ liệu lớn ở Việt Nam, một trong con số hiếm hoi đối tác chính thức của Google ở khu vực Đông Nam Á.',
                    'description_2'=>'Google hợp tác với ICOM để cung cấp 3 nền tảng:
- Nền tảng quản lý Nhà quảng cáo & Nhà phát hành nội dung (Google DFP / AdExchange)
- Nền tảng Business Intelligence
- Wifi marketing',
                    'content'=>'<ul><li>1. Nền tảng quản lý Nhà quảng cáo & Nhà phát hành nội dung (Google DFP / AdExchange)
Google AdX là nền tảng quảng cáo trung gian mới ra mắt của Google hoạt động trên nguyên lý Programatic Ads (quảng cáo được lập trình tự động) và RTB (đấu giá thời gian thực), giúp kết nối doanh nghiệp quảng cáo và các nhà phát triển nội dung.

Với kinh nghiệm triển khai các dịch vụ trực tuyến và quảng cáo trực tuyến, ICOM sẽ giúp doanh nghiệp:
- Tư vấn, đưa ra những giải pháp quảng cáo trực tuyến tốt nhất, tối ưu nhất nhằm mang lại hiệu quả kinh doanh cao cho doanh nghiệp
- Thiết lập chiến dịch quảng cáo tối ưu nhất cho mục đích quảng cáo của doanh nghiệp
- Theo dõi, báo cáo kết quả chiến dịch marketing sao cho phù hợp với mục đích quảng cáo của doanh nghiệp theo từng thời điểm
Với đội ngũ nhân viên chuyên nghiệp có nhiều năm kinh nghiệm trong lĩnh vực quảng cáo trực tuyến, luôn lao động hết mình vì đam mê, cùng với tinh thần trách nhiệm cao nhất để phục vụ khách hàng, sẵn sàng hỗ trợ và tư vấn cho khách hàng 24/7</li>

<li>2. Nền tảng Business Intelligence
Big Data là nhu cầu đang tăng trưởng lớn đến nỗi Software AG, Oracle, IBM, Microsoft, SAP, EMC, HP và Dell đã chi hơn 15 tỉ USD cho các công ty chuyên về quản lí và phân tích dữ liệu. Năm 2010, ngành công nghiệp Big Data có giá trị hơn 100 tỉ USD và đang tăng nhanh với tốc độ 10% mỗi năm, nhanh gấp đôi so với tổng ngành phần mềm nói chung. Trong nền tảng Business Intelligence, ICOM kết hợp hành vi người dùng thu thập bởi Google và dữ liệu thu thập bởi ICOM để xây dựng Big Data. Big Data được sử dụng để tối ưu hoá các hoạt động bán hàng và marketing, cắt giảm chi phí và thời gian tối ưu hoá sản phẩm đồng thời giúp các doanh nghiệp đưa ra quyết định đúng đắn và kịp thời hơn.</li>

<li>3. Wifi Marketing
ICOM đang phối hợp với các nhà mạng ở Việt Nam để cung cấp wifi miễn phí ở các khu vực nông thôn ở Việt Nam thông qua giải pháp của Google. Nhờ dịch vụ này, người dân ở vùng sâu vùng xa có thể tiếp cận với Internet dễ dàng, từ đó nâng cao kiến thức chất lượng đời sống của họ. Ngoài ra, Wifi marketing cũng mở ra cơ hội cho các doanh nghiệp, nhãn hàng tiếp cận tới nhiều khách hàng hơn.</li><ul>',
                'thumbnail'=>'/pictures/service06.png',
                'banner_thumb'=>'/pictures/banner_marketing.jpg'
                ]
            ],
            str_slug('GIẢI PHÁP THANH TOÁN')=>[
                'en'=>[
                    'title'=>'Giải pháp thanh toán',
                    'description'=>'',
                    'description_2'=>'',
                    'content'=>'',
                    'thumbnail'=>'/pictures/service06.png',
                    'banner_thumb'=>'/pictures/banner_paysolution.jpg'
                ],
                'vi'=>[
                    'title'=>'Giải pháp thanh toán',
                    'description'=>'Trên thế giới và đặc biệt tại Việt Nam, hầu hết các thuê bao di động đều là thuê bao trả trước, theo thống kê tại nhiều đất nước có thể có tới trên 99% thuê bao là trả trước. Với đặc thù riêng này, có số lượng lớn thuê bao bị hết tiền mà chưa thể nạp ngay gây lãng phí và thất thoát nguồn doanh thu cho nhà mạng và phiền hà cho khách hàng vì không thể sử dụng được dịch vụ. Nhận thức được vấn đề này, ICOM đã hợp tác với những nhà mạng lớn ở Việt Nam để phát triển dịch vụ ứng tiền, chuyển tiền nhanh chóng và tiện lợi giữa các thuê bao di động.',
                    'description_2'=>'Năm 2016, ICOM thành lập Trung tâm Giải pháp thanh toán với trọng tâm là phát triển các giải pháp về tài chính và thành toán cũng như cung cấp dịch vụ Ví điện tử tới thuê bao di động. Chúng tôi mong muốn mang lại cho người dùng trải nghiệm

NHANH HƠN – DỄ DÀNG HƠN – TỐT HƠN

khi thực hiện các giao dịch trực tuyến như mua bán trực tuyến, games, dịch vụ giá trị gia tăng.',
                    'content'=>'Dịch vụ Ứng tiền cung cấp cho khách hàng một phương thức ứng trước tiền điện thoại khi hết tiền trong tài khoản mà chưa thể nạp được ngay. Lượng tiền đã ứng và phí dịch vụ sẽ được thu hồi trong lần nạp tiền tiếp theo của khách hàng. Số tiền ứng trước này sẽ được nạp trực tiếp vào tài khoản của khách hàng và thông báo được gửi về qua SMS từ đầu số dịch vụ. Ngoài ra, khách hàng có thể sử dụng số tiền đó quy đổi ra các dịch vụ giá trị gia tăng sử dụng trên điện thoại như data, thoại, SMS, thẻ cào…

Dịch vụ Chuyển Tiền là một dịch vụ tiện ích giúp các thuê bao di động trả trước chuyển tiền trong tài khoản chính cho nhau thao tác trực tiếp trên máy điện thoại di mà không mất cước đăng ký; cước tin nhắn, cước thuê bao tháng. Ngoài tính năng nổi bật trên, dịch vụ Chuyển tiền còn hỗ trợ các tính năng khác như gửi yêu cầu chuyển tiền, gửi lời nhắn, thiệp, hình ảnh hoàn toàn miễn phí cho bạn bè người thân và cùng nhau tham gia các chương trình khuyến mại giành giải thưởng.
Với những thao tác đơn giản trên thiết bị di động cá nhân, khách hàng sẽ có ngay những trải nghiệm tiện ích vô cùng tuyệt vời khi đến với dịch vụ Chuyển tiền.

Giải pháp ví điện tử (E-wallet) của ICOM giúp hàng triệu thuê bao di động dùng thanh toán online một cách dễ dàng và tiện lợi thông qua điện thoại. Ví điện tử là dịch vụ thanh toán trên di động, dưới hình thức Ví điện tử di động, cho phép thanh toán hóa đơn, nạp tiền điện thoại, thực hiện các thao tác chuyển tiền, rút riền từ tài khoản ngân hàng, giữa ví và ví.',
                'thumbnail'=>'/pictures/service06.png',
                'banner_thumb'=>'/pictures/banner_paysolution.jpg'
                ]
            ],
            str_slug('NỘI DUNG SỐ')=>[
                'en'=>[
                    'title'=>'Nội dung số',
                    'description'=>'',
                    'description_2'=>'Emdep.vn tự hào là tờ báo điện tử hàng đầu về Phụ nữ, là nơi để phái đẹp tìm hiểu thông tin, chia sẻ bí quyết, tâm sự, tình cảm về cuộc sống tình yêu, hôn nhân, gia đình. Đồng thời cũng là nơi để chị em tìm hiểu các thông tin về thời trang, làm đẹp, chăm sóc bé để khiến cuộc sống luôn tươi trẻ.',
                    'content'=>'THƯƠNG HIỆU HÀNG ĐẦU VỀ PHỤ NỮ
Hơn 7 triệu lượt truy cập mỗi tháng
ĐƯỢC QUAN TÂM HÀNG ĐẦU TRÊN FACEBOOK
12 Fanpage vệ tinh với hơn 3 triệu Fans
YOUTUBE
Dành được nút bạc Youtube năm 2016 với hơn 131.000 người
theo dõi

03 NĂM xây dựng và phát triển
Tạo môi trường GẮN BÓ với độc giả
Xây dựng CỘNG ĐỒNG CHUNG lớn mạnh trên MXH

Truy cập: Emdep.vn để tìm hiểu thêm',
                    'thumbnail'=>'/pictures/thumb_digital.jpg',
                    'banner_thumb'=>'/pictures/banner_digital.jpg'
                ],
                'vi'=>[
                    'title'=>'Nội dung số',
                    'description'=>'ICOM là một trong số ít công ty ở Việt Nam được cấp giấy phép sở hữu trang báo điện tử.

Emdep.vn tự hào là tờ báo điện tử hàng đầu về Phụ nữ, là nơi để phái đẹp tìm hiểu thông tin, chia sẻ bí quyết, tâm sự, tình cảm về cuộc sống tình yêu, hôn nhân, gia đình. Đồng thời cũng là nơi để chị em tìm hiểu các thông tin về thời trang, làm đẹp, chăm sóc bé để khiến cuộc sống luôn tươi trẻ.',
                    'description_2'=>'',
                    'content'=>'',
                'thumbnail'=>'/pictures/thumb_digital.jpg',
                'banner_thumb'=>'/pictures/banner_digital.jpg'
                ]
            ],
            str_slug('Adsblock')=>[
                'en'=>[
                    'title'=>'AdsBlock Solution',
                    'description'=>'ICOM là một trong số ít công ty ở Việt Nam được cấp giấy phép sở hữu trang báo điện tử.

Emdep.vn tự hào là tờ báo điện tử hàng đầu về Phụ nữ, là nơi để phái đẹp tìm hiểu thông tin, chia sẻ bí quyết, tâm sự, tình cảm về cuộc sống tình yêu, hôn nhân, gia đình. Đồng thời cũng là nơi để chị em tìm hiểu các thông tin về thời trang, làm đẹp, chăm sóc bé để khiến cuộc sống luôn tươi trẻ.',
                    'description_2'=>'',
                    'content'=>'',
                'thumbnail'=>'/pictures/service06.png',
                'banner_thumb'=>'/pictures/banner_adsblock.jpg'
                ],
                'vi'=>[
                    'title'=>'Giải pháp chặn quảng cáo',
                    'description'=>'Với sự phát triển của Internet, ngoài việc người dùng tiếp cận được với nhiều thông tin thì bên cạnh đó vẫn có những mặt trái như thông tin không lành mạnh, không phù hợp với độ tuổi, hay quảng cáo tràn lan khiến người dùng thấy phản cảm, và gây giảm tốc độ truy cập… Giải pháp Chặn quảng cáo của ICOM được phát triển để hạn chế tối đa những bất cập khi truy cập Internet và tối ưu các tính năng góp phần tăng trải nghiệm tích cực cho người sử dụng.',
                    'description_2'=>'"Giải pháp Chặn quảng cáo bảo vệ bạn ngay từ hạ tầng mạng nên chất lượng dịch vụ luôn ổn định và bảo vệ người dùng triệt để. Bạn không cần cài đặt phần mềm để sử dụng dịch vụ, không bị phụ thuộc vào ứng dụng của bên thứ 3 giúp thiết bị và thông tin của bạn luôn luôn được an toàn và bảo mật. Giải pháp Chặn quảng cáo sẽ giúp bạn tránh bị làm phiền bởi quảng cáo, những thông tin không lành mạnh, đồng thời giúp bạn tiết kiệm được chi phí 3G và tăng tốc độ duyệt web',
                    'content'=>'<p>Giải pháp Chặn quảng cáo bao gồm 3 dịch vụ:</p>
                    <ul>
                        <li>- Chặn quảng cáo (Adsblock): Dịch vụ tự động chặn hiệu quả tất cả quảng cáo banner, video, popup,...trên trình duyệt và trong ứng dụng điện thoại</li>
                        <li>- Quản lý truy cập Internet: Dịch vụ giúp cha mẹ hoàn toàn yên tâm khi cho con truy cập Internet. Dịch vụ này ngăn chặn những nội dung, trang web độc hại bằng cách tự động phát hiện và ngay lập tức điều hướng trẻ nhỏ tới trang thông báo của dịch vụ khi trẻ nhỏ vô tình truy cập những website xấu.</li>
                        <li>- Cảnh báo nội dung xấu: Dịch vụ tự động phát hiện và đưa chúng ta ra khỏi những tranh web độc hại, đảm bảo rằng người dùng có 1 môi trường internet lành mạnh.</li>
                    </ul>
                    <p>Để biết thêm thông tin, vui lòng truy cập website: adsblock.vn</p>',
                'thumbnail'=>'/pictures/service06.png',
                'banner_thumb'=>'/pictures/banner_adsblock.jpg'
                ]
            ]
        ];
    }

    public function index(){
        return view('sites.service',['m_act'=>$this->_mact,'menu'=>$this->_menu]);
    }

    public function detail($params){
        $detail = [];
        if (array_key_exists($params, $this->_a_services)) {
            $detail = $this->_a_services[$params][$this->_locale];
        }
        $detail['value'] = 2;
        if($params==str_slug('DỊCH VỤ GIÁ TRỊ GIA TĂNG')) {$detail['value']=1;}
        // dd($detail);
        return view('sites.servi_detail',['m_act'=>$this->_mact,'menu'=>$this->_menu,'detail'=>$detail,'slug'=>$params]);
    }
}
