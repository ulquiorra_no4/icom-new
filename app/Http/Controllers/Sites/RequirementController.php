<?php

namespace App\Http\Controllers\Sites;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\AppController;
use App\Models\Posts\PostRepository;
class RequirementController extends AppController
{
    //
    protected $_mact;
    protected $_postRepository;
    public function __construct(PostRepository $postRepository){
        parent::__construct();
        $this->_mact = 4;
        $this->_postRepository = $postRepository;
    }

    public function index(Request $request){
        $o_post = $this->_postRepository->getList(['category_id'=>12]);
        $keyword = '';
        // dd($o_post);
        if ($request->input('job')) {
            $keyword = $request->input('job');
            $o_post = $this->_postRepository->searchPost($keyword,['category_id'=>12]);
            // dd($o_post);
        }

        return view('sites.requirement',['m_act'=>$this->_mact,'a_data'=>$o_post,'menu'=>$this->_menu,'keyword'=>$keyword]);
    }

    public function detail($params){
        $params = trim($params);
        $detail = $this->_postRepository->findBySlug($params);
        // dd($detail);
        // echo $id;die;
        return view('sites.requi_detail',['m_act'=>$this->_mact,'menu'=>$this->_menu,'detail'=>$detail]);
    }
}
