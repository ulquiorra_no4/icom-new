<?php

namespace App\Http\Controllers\Sites;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\AppController;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Input;
use App;
use Lang;
use Illuminate\Http\Response;
use App\News;
use Illuminate\Support\Facades\DB;
class IndexController extends AppController
{
    //
    protected $_mact;

    public function __construct(Request $request)
    {
        parent::__construct();
        $this->_mact = 0;
    }
    public function index(Request $request)
    {
    	return view('sites.index',['m_act'=>$this->_mact,'menu'=>$this->_menu]);
    }

    public function language_set(Request $request){
        if($request->ajax()){
            $request->session()->put('locale',$request->locale);
        }
    }
    public function get_posts(){
        $data = News::all()->toArray();

        $insert = array_map(function($arr){

            $return['id'] = $arr['news_id'];
            $return['title'] = $arr['news_title'];
            $return['title_en'] = $arr['news_title_en'];
            $return['slug'] = str_slug($arr['news_title']);
            $return['description'] = $arr['news_summary'];
            $return['description_en'] = $arr['news_summary_en'];
            $return['content'] = $arr['news_content'];
            $return['content_en'] = $arr['news_content'];
            $return['category_id'] = $arr['news_cat_id'];
            $return['group_id'] = $arr['news_group_id'];
            $return['avatar'] = $arr['news_image'];
            $return['active'] = $arr['news_active'];
            $return['salary'] = $arr['news_salary'];
            $return['place'] = $arr['news_place'];
            $return['position'] = $arr['news_id'];
            $return['position_en'] = $arr['news_id'];
            $return['view'] = $arr['news_view'];
            $return['published_at'] = date('Y-m-d H:i:s',$arr['news_date_modify']);
            $return['author_id'] = 1;
            return $return;
        },$data);

        dd($insert);
        DB::table('posts')->insert($insert);
    }

}
