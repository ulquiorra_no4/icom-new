<?php

namespace App\Http\Controllers\Sites;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\AppController;
use App\Models\Posts\PostRepository;
class NewsController extends AppController
{
    //
    protected $_mact;
    protected $_postRepository;
    public function __construct(PostRepository $postRepository){
        parent::__construct();
        $this->_mact = 3;
        $this->_postRepository = $postRepository;
    }

    public function index(){
        $o_post = $this->_postRepository->getList([['category_id','<>',12]]);
        // dd($o_post);
        return view('sites.news',['m_act'=>$this->_mact,'menu'=>$this->_menu,'a_data'=>$o_post]);
    }

    public function detail($params){
        $params = trim($params);
        $relation = [];
        $latest = $this->_postRepository->getList([['category_id','<>',12]],5);
        $detail = $this->_postRepository->findBySlug($params);
        // Get post relation
        if($detail){
            $relation = $this->_postRepository->getList(['category_id'=>$detail->category_id],5);
        }

        // dd($detail);
        return view('sites.news_detail',['m_act'=>$this->_mact,'menu'=>$this->_menu,'detail'=>$detail,'relation'=>$relation,'latest'=>$latest]);
    }
}
