<?php
/**
 * Created by PhpStorm.
 * User: HiepHM
 * Date: 11/16/2017
 * Time: 1:33 PM
 */
namespace App\Console\Commands;
use Illuminate\Console\Command;
use File;
class RepositoryCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:repository
        {name : Repository name}
        {--model= : (Optional) Model name}
    ';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create repository for model';
    private $repositoryPath;
    private $modelPath;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        // $this->repositoryPath = config('repository.repository_path', 'app/Repositories/');
        $this->repositoryPath = config('repository.repository_path', 'app/Models/');
        $this->modelPath = config('repository.model_path', 'App\Models\\');
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $repositoryContent = '';
        $modelName = $this->option('model');
        $firstName = $this->argument('name');
        $interfaceName = $firstName.'Interface';
        $repositoryName = $firstName.'Repository';

        // $repositoryPath = sprintf('%s/%s.php', trim($this->repositoryPath, '/'), $repositoryName);
        $repositoryPath = sprintf('%s/%s.php', $this->repositoryPath.$modelName, $repositoryName);
        $interfacePath = sprintf('%s/%s.php',$this->repositoryPath.$modelName, $interfaceName);
        if ($repositoryName) {
            $repositoryContent = $this->getRepositoryContent($repositoryName, $interfaceName, $modelName);
            $interfaceContent = $this->getInterfaceContent($interfaceName, $modelName);
        }
        if (!File::isFile($repositoryPath)) {
            $this->setRepositoryContent($repositoryPath, $repositoryContent);
        } else {
            $overrideConfirm = $this->ask('Repository is existing. Are you sure you want to override it? (y/n)');
            $yesAnswerList = ['y', 'yes'];
            if (in_array(strtolower($overrideConfirm), $yesAnswerList)) {
                $this->setRepositoryContent($repositoryPath, $repositoryContent);
            }
        }

        if (!File::isFile($interfacePath)) {
            $this->setInterfaceContent($interfacePath, $interfaceContent);
        } else {
            $overrideConfirm = $this->ask('Interface is existing. Are you sure you want to override it? (y/n)');
            $yesAnswerList = ['y', 'yes'];
            if (in_array(strtolower($overrideConfirm), $yesAnswerList)) {
                $this->setInterfaceContent($interfacePath, $interfaceContent);
            }
        }
    }
    protected function setRepositoryContent($repositoryPath, $repositoryContent)
    {
        File::put($repositoryPath, $repositoryContent);
        $this->info('Repository created successfully');
    }
    protected function getRepositoryContent($repositoryName, $interfaceName, $modelName = '')
    {
        $repositoryContent = "<?php
namespace App\Models\\$modelName;
use App\Models\BaseInterface;
class {$repositoryName} implements {$interfaceName}, BaseInterface
{";
        if ($modelName) {
            $repositoryContent .= "
    protected \$_model;
    public function __construct($modelName \$model)
    {
        \$this->_model = \$model;
    }";
        }
        $repositoryContent .= "
}";
        return $repositoryContent;
    }
    protected function setInterfaceContent($interfacePath, $interfaceContent)
    {
        File::put($interfacePath, $interfaceContent);
        $this->info('Interface created successfully');
    }
    protected function getInterfaceContent($interfaceName, $model = '')
    {
        $path = $model;
        $repositoryContent = "<?php
namespace App\Models\\$path;
interface ".$interfaceName."{
}";

        return $repositoryContent;
    }
}