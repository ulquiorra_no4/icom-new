<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    //
    protected $table = 'news';

    protected $primaryKey = 'news_id';
    protected $fillable = [
        'news_title',
        'news_title_en',
        'news_summary',
        'news_summary_en',
        'news_content',
        'news_content_en',
        'news_date_create',
        'news_date_modify',
        'news_active',
        'news_home',
        'news_cat_id',
        'news_image',
        'news_view',
        'news_salary',
        'news_place',
        'news_group_id',
        'user_id'
    ];
}
