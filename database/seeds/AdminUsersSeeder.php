<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class AdminUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('admin_users')->insert([
            'name' => 'Ulquiorra Schiffer',
            'username' => 'ulquiorra',
            'email' => 'testulquiorra@gmail.com',
            'phone' => '01686868686',
            'address' => 'Ha Loi Viet Nam',
            'password' => bcrypt('123qwe'),
            'role' => 'administrator',
            'active' => 1
        ]);
        // DB::table('admin_users')->insert([
        //     'name' => 'Thảo Quách',
        //     'username' => 'thaoqp',
        //     'email' => 'thaoqp@i-com.vn',
        //     'phone' => '696969',
        //     'address' => 'Ha Loi Viet Nam',
        //     'password' => bcrypt('thao123'),
        //     'role' => 'admin',
        //     'active' => 1
        // ]);
    }
}
