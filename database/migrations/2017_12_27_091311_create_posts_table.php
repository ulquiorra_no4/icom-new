<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('title_en');
            $table->string('slug')->nullable();
            $table->text('description')->nullable();
            $table->text('description_en')->nullable();
            $table->longText('content')->nullable();
            $table->longText('content_en')->nullable();
            $table->string('keywords')->nullable();
            $table->integer('category_id')->default(0);
            $table->integer('group_id')->default(0);
            $table->string('cate_tags')->nullable();
            $table->string('tags')->nullable();
            $table->string('avatar',100)->nullable();
            $table->enum('stage',['draft','queue','trash','publish'])->default('draft');
            $table->tinyInteger('active')->default(0);
            $table->integer('active_time')->nullable();
            $table->string('option')->nullable();
            $table->integer('author_id')->unsigned();
            $table->string('source',100)->nullable();
            $table->string('source_url')->nullable();
            $table->integer('view')->default(0);
            $table->integer('partner_id')->default(0);
            $table->string('salary',100)->nullable();
            $table->string('place')->nullable();
            $table->string('position')->nullable();
            $table->string('position_en')->nullable();
            $table->timestamp('end_time')->nullable();
            $table->timestamp('published_at')->nullable();
            $table->timestamps();

            $table->foreign('author_id')
                    ->references('id')
                    ->on('admin_users')
                    ->onDelete('cascade');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
