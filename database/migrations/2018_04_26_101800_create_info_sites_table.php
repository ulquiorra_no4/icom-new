<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInfoSitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('info_sites', function (Blueprint $table) {
            $table->increments('inf_id');
            $table->string('inf_title')->nullable();
            $table->string('inf_title_en')->nullable();
            $table->string('inf_description')->nullable();
            $table->string('inf_description_en')->nullable();
            $table->integer('inf_page_id');
            $table->tinyInteger('inf_isSub')->default(0);
            $table->tinyInteger('active')->default(0);
            $table->enum('inf_position',['banner','top','mid','bottom'])->default('mid');
            $table->integer('order')->default(99);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('info_sites');
    }
}
