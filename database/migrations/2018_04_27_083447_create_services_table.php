<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->increments('ser_id');
            $table->string('ser_title')->unique();
            $table->string('ser_title_en');
            $table->string('slug')->unique();
            $table->text('ser_description')->nullable();
            $table->text('ser_description_en')->nullable();
            $table->text('ser_quote')->nullable();
            $table->text('ser_quote_en')->nullable();
            $table->text('ser_content')->nullable();
            $table->text('ser_content_en')->nullable();
            $table->string('avatar',100)->nullable();
            $table->string('image_thumb',100)->nullable();
            $table->tinyInteger('active')->default(1);
            $table->tinyInteger('hot')->default(0);
            $table->tinyInteger('set_home')->dafault(0);
            $table->integer('order')->default(99);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
