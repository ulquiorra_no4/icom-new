<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menuses', function (Blueprint $table) {
            $table->increments('menu_id');
            $table->string('menu_name',100)->unique();
            $table->string('menu_name_en',100)->nullable();
            $table->string('menu_alias',100)->nullable();
            $table->string('menu_link')->default('/');
            $table->tinyInteger('active')->default(0);
            $table->string('menu_icon')->nullable();
            $table->integer('order')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menuses');
    }
}
