<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('cat_id');
            $table->string('cat_name',30)->unique();
            $table->string('cat_name_en',30)->nullable();
            $table->string('cat_alias',30)->nullable();
            $table->string('cat_description')->nullable();
            $table->integer('cat_parent_id')->default(0);
            $table->integer('cat_type')->default(0);
            $table->tinyInteger('cat_has_child')->default(0);
            $table->tinyInteger('active')->default(0);
            $table->integer('order')->default(9999);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
