<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('admin_users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name',50);
            $table->string('username',50)->unique();
            $table->string('email',100)->unique();
            $table->string('phone',20)->nullable();
            $table->string('address')->nullable();
            $table->enum('role',['administrator','admin','poster','member'])->default('member');
            $table->integer('group')->default(0);
            $table->string('password',100);
            $table->string('avatar',100)->nullable();
            $table->tinyInteger('active')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('admin_users');
    }
}
